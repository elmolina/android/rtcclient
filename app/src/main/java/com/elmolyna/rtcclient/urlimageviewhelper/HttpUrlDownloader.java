package com.elmolyna.rtcclient.urlimageviewhelper;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

import android.content.Context;
import android.os.AsyncTask;

import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.oslc.OSLCConnection;
import com.elmolyna.rtcclient.urlimageviewhelper.UrlImageViewHelper.RequestPropertiesCallback;

public class HttpUrlDownloader implements UrlDownloader {
    private RequestPropertiesCallback mRequestPropertiesCallback;

    public RequestPropertiesCallback getRequestPropertiesCallback() {
        return mRequestPropertiesCallback;
    }

    public void setRequestPropertiesCallback(final RequestPropertiesCallback callback) {
        mRequestPropertiesCallback = callback;
    }


    @Override
    public void download(final Context context, final String url, final String filename, final UrlDownloaderCallback callback, final Runnable completion) {
        final AsyncTask<Void, Void, Void> downloader = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(final Void... params) {
                try {
                    RTCClientApplication appState = ((RTCClientApplication) context.getApplicationContext());
                    HttpGet doc = new HttpGet(url);
                    
                    HttpResponse response = OSLCConnection.sendGetForSecureDocument(appState.getConnectionUrl(), doc, appState.getUsername(), appState.getPassword(), appState.getHttpClient());

        			if (response.getStatusLine().getStatusCode() == 200) {
        				callback.onDownloadComplete(HttpUrlDownloader.this, response.getEntity().getContent(), null);
        			}
                    
                    return null;
                }
                catch (final Throwable e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(final Void result) {
                completion.run();
            }
        };

        UrlImageViewHelper.executeTask(downloader);
    }

    @Override
    public boolean allowCache() {
        return true;
    }
    
    @Override
    public boolean canDownloadUrl(String url) {
        return url.startsWith("http");
    }
}
