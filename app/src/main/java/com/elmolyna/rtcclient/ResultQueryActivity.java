package com.elmolyna.rtcclient;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.elmolyna.rtcclient.fragments.ResultQueryFragment;
import com.elmolyna.rtcclient.generic.RTCAnimationActivity;

public class ResultQueryActivity extends RTCAnimationActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.result_query_fragment);
		// Show the Up button in the action bar.

		if (savedInstanceState == null) {
			FragmentManager fm = getSupportFragmentManager();
			Fragment fragment = new ResultQueryFragment();
			FragmentTransaction transaction = fm.beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			transaction.add(R.id.result_query_fragment, fragment, "resultQueryFragment");
			transaction.commit();
		}
	}
}
