package com.elmolyna.rtcclient;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.elmolyna.rtcclient.fragments.WorkItemTabFragment;
import com.elmolyna.rtcclient.generic.RTCAnimationActivity;

public class WorkItemActivity extends RTCAnimationActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.workitem_fragment);
		// Show the Up button in the action bar.
		
		if (savedInstanceState == null) {				   
			FragmentManager fm = getSupportFragmentManager();
			WorkItemTabFragment fragment = new WorkItemTabFragment();
			FragmentTransaction transaction = fm.beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			transaction.add(R.id.workitem_tab_fragment, fragment, "workItemTabFragment");
			transaction.commit();
		}
	}
}
