package com.elmolyna.rtcclient.generic;

import android.support.v4.content.AsyncTaskLoader;
import android.content.Context;

import com.elmolyna.rtcclient.data.RTCData;

public abstract class RTCLoader<D> extends AsyncTaskLoader<RTCData<D>> {

	public static final String SAVE_KEY = "SAVE_KEY";
	
	private RTCData<D> data = null;
	
	public RTCLoader(Context context) {
		super(context);
	}

	public RTCData<D> getData() {
		return data;
	}

	public void setData(RTCData<D> data) {
		this.data = data;
	}

	/**
	 * This is where the bulk of our work is done. This function is called in a background thread and should generate a new set of data to be published by the
	 * loader.
	 */
	@Override
	public abstract RTCData<D> loadInBackground();

	/**
	 * Called when there is new data to deliver to the client. The super class will take care of delivering it; the implementation here just adds a little more
	 * logic.
	 */
	@Override
	public void deliverResult(RTCData<D> data) {
		if (isReset()) {
			// An async query came in while the loader is stopped. We don't need the result.
			if (data != null) {
				onReleaseResources(data);
			}
		}
		RTCData<D> oldApps = data;
		this.data = data;

		if (isStarted()) {
			// If the Loader is currently started, we can immediately
			// deliver its results.
			super.deliverResult(data);
		}

		// At this point we can release the resources associated with
		// 'oldApps' if needed; now that the new result is delivered we
		// know that it is no longer in use.
		if (oldApps != null) {
			onReleaseResources(oldApps);
		}
	}

	/**
	 * Handles a request to start the Loader.
	 */
	@Override
	protected void onStartLoading() {
		if (data != null) {
			// If we currently have a result available, deliver it
			// immediately.
			deliverResult(data);
		}

		if (takeContentChanged() || data == null) {
			// If the data has changed since the last time it was loaded
			// or is not currently available, start a load.
			forceLoad();
		}
	}

	/**
	 * Handles a request to stop the Loader.
	 */
	@Override
	protected void onStopLoading() {
		// Attempt to cancel the current load task if possible.
		cancelLoad();
	}

	/**
	 * Handles a request to cancel a load.
	 */
	@Override
	public void onCanceled(RTCData<D> queries) {
		super.onCanceled(queries);

		// At this point we can release the resources associated with 'queries' if needed.
		onReleaseResources(queries);
	}

	/**
	 * Handles a request to completely reset the Loader.
	 */
	@Override
	protected void onReset() {
		super.onReset();

		// Ensure the loader is stopped
		onStopLoading();

		// At this point we can release the resources associated with 'apps'
		// if needed.
		if (data != null) {
			onReleaseResources(data);
			data = null;
		}
	}

	/**
	 * Helper function to take care of releasing resources associated with an actively loaded data set.
	 */
	protected void onReleaseResources(RTCData<D> apps) {		
		// For a simple List<> there is nothing to do. For something
		// like a Cursor, we would close it here.
	}
}
