package com.elmolyna.rtcclient.generic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.SearchView.OnQueryTextListener;

import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.ResultQueryActivity;
import com.elmolyna.rtcclient.generic.ActivityListener;

public class RTCFragment extends Fragment implements OnQueryTextListener {

	// Communication with activity
	private ActivityListener activityListener;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			this.activityListener = (ActivityListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement ActivityListener");
		}
	}
	
	public void showErrorDialog(int title, String message) {
		activityListener.showErrorDialog(title, message);
	}
	
	/**
	 * 
	 * SEARCH
	 * 
	 */
	// The SearchView for doing filtering.
	private SearchView mSearchView;

	@Override
	public boolean onQueryTextSubmit(String query) {
		RTCClientApplication appState = ((RTCClientApplication) getActivity().getApplicationContext());
		String simpleQueryURL = appState.getServices().getSimpleQuery() + "?oslc_cm.query=oslc_cm:searchTerms=%22%2A" + query + "%2A%22&";
		appState.setQueryURL(simpleQueryURL);

		Intent intent = new Intent(this.getActivity(), ResultQueryActivity.class);
		startActivity(intent);
		return true;
	}

	@Override
	public boolean onQueryTextChange(String arg0) {
		return false;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		// Place an action bar item for searching.
		MenuItem item = menu.add("Search");
		item.setIcon(android.R.drawable.ic_menu_search);
		item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		mSearchView = new MySearchView(getActivity());
		mSearchView.setOnQueryTextListener(this);
		mSearchView.setIconifiedByDefault(true);
		item.setActionView(mSearchView);

		// Applies white color on searchview text
		int id = mSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
		TextView textView = (TextView) mSearchView.findViewById(id);
		textView.setTextColor(Color.WHITE);
	}

	public static class MySearchView extends SearchView {
		public MySearchView(Context context) {
			super(context);
		}

		// The normal SearchView doesn't clear its search text when
		// collapsed, so we will do this for it.
		@Override
		public void onActionViewCollapsed() {
			setQuery("", false);
			// super.onActionViewCollapsed();
		}
	}
}
