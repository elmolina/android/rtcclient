package com.elmolyna.rtcclient.generic;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

public class RTCAsyncFragment extends RTCFragment {

	static final int INTERNAL_PROGRESS_CONTAINER_ID = 0x00ff0001;
	static final int INTERNAL_CONTENT_CONTAINER_ID = 0x00ff0002;

	View mProgressContainer;
	View mContentContainer;
	boolean mContentShown = true;

	public View onCreateAsyncView(View view) {
		return onCreateAsyncView(view, android.R.attr.progressBarStyleSmall);
	}

	public View onCreateAsyncView(View view, int progressBarStyle) {

		final Context context = getActivity();

		FrameLayout rootFrameLayout = new FrameLayout(context);

		// ------------------------------------------------------------------

		LinearLayout linearLayoutProgress = new LinearLayout(context);
		linearLayoutProgress.setId(INTERNAL_PROGRESS_CONTAINER_ID);
		linearLayoutProgress.setOrientation(LinearLayout.VERTICAL);

		linearLayoutProgress.setGravity(Gravity.CENTER);

		ProgressBar progress = new ProgressBar(context, null, progressBarStyle);
		linearLayoutProgress.addView(progress, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

		rootFrameLayout.addView(linearLayoutProgress, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

		// ------------------------------------------------------------------

		FrameLayout contentFrameLayout = new FrameLayout(context);
		contentFrameLayout.setId(INTERNAL_CONTENT_CONTAINER_ID);
		contentFrameLayout.addView(view);
		contentFrameLayout.setVisibility(View.GONE);

		rootFrameLayout.addView(contentFrameLayout, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

		// ------------------------------------------------------------------

		rootFrameLayout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

		return rootFrameLayout;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mProgressContainer = getView().findViewById(INTERNAL_PROGRESS_CONTAINER_ID);
		mContentContainer = getView().findViewById(INTERNAL_CONTENT_CONTAINER_ID);
		setContentShown(false, false);
	}

	@Override
	public void onDestroyView() {
		mContentShown = false;
		mProgressContainer = mContentContainer = null;
		super.onDestroyView();
	}

	public void setContentShown(boolean shown) {
		setContentShown(shown, true);
	}

	public void setContentShownNoAnimation(boolean shown) {
		setContentShown(shown, false);
	}

	private void setContentShown(boolean shown, boolean animate) {

		mProgressContainer = getView().findViewById(INTERNAL_PROGRESS_CONTAINER_ID);
		mContentContainer = getView().findViewById(INTERNAL_CONTENT_CONTAINER_ID);

		if (mContentShown == shown) {
			return;
		}
		mContentShown = shown;
		if (shown) {
			if (animate) {
				mProgressContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_out));
				mContentContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in));
			} else {
				mProgressContainer.clearAnimation();
				mContentContainer.clearAnimation();
			}
			mProgressContainer.setVisibility(View.GONE);
			mContentContainer.setVisibility(View.VISIBLE);
		} else {
			if (animate) {
				mProgressContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in));
				mContentContainer.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_out));
			} else {
				mProgressContainer.clearAnimation();
				mContentContainer.clearAnimation();
			}
			mProgressContainer.setVisibility(View.VISIBLE);
			mContentContainer.setVisibility(View.GONE);
		}
	}
}
