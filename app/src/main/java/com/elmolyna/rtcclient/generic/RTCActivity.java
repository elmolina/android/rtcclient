package com.elmolyna.rtcclient.generic;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.elmolyna.rtcclient.R;

public class RTCActivity extends FragmentActivity implements ActivityListener {
		
	// UserQueriesListener
	@Override
	public void showErrorDialog(int title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(message).setTitle(title);
		builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	/**
	 * Permitir back navegaciones en navegaciones de fragmentos
	 */
	@Override
	public boolean onNavigateUp() {
	    FragmentManager fm = getSupportFragmentManager();
        if(fm.getBackStackEntryCount()>0){
        	fm.popBackStack();
        }
        else {
			finish();
		}
		return true;
	}
}
