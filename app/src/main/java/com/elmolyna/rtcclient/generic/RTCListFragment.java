package com.elmolyna.rtcclient.generic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.ResultQueryActivity;
import com.elmolyna.rtcclient.data.RTCData;
import com.elmolyna.rtcclient.data.RTCResult;

public abstract class RTCListFragment <T> extends ListFragment implements OnQueryTextListener, OnScrollListener, LoaderManager.LoaderCallbacks<RTCData<T>> {

	// OLDTODO paginacion inversa -> resuelto con carga siempre
	// TODO loading al final de lista infinita
	
	// This is the Adapter being used to display the list's data.
	protected ArrayAdapter<T> adapter;
	
	// Communication with activity
	private ActivityListener activityListener;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			this.activityListener = (ActivityListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement ActivityListener");
		}
	}
	
	public void showErrorDialog(int title, String message) {
		activityListener.showErrorDialog(title, message);
	}
	
	private String next = null;

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		if (view.getAdapter() != null) {
			if (((firstVisibleItem + visibleItemCount) >= totalItemCount) && next != null) {
				Bundle bundle = new Bundle();
				bundle.putString("NEXT", next);
				next = null;
				getLoaderManager().restartLoader(0, bundle, this);
			}
		}
	}
	
	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// do nothing
	}
	
	// LoaderManager.LoaderCallbacks
	@Override
	public void onLoadFinished(Loader<RTCData<T>> loader, RTCData<T> data) {

		if (data.getResult() != RTCResult.OK) {
			showErrorDialog(data.getTitle(), data.getMessage());
		} else {
			// Set the new data in the adapter.
			adapter.addAll(data.getList());
			this.next = data.getNext();
		}

		// The list should now be shown.
		if (isResumed()) {
			setListShown(true);
		} else {
			setListShownNoAnimation(true);
		}
	}
	
	@Override
	public abstract Loader<RTCData<T>> onCreateLoader(int arg0, Bundle arg1);

	// LoaderManager.LoaderCallbacks
	@Override
	public void onLoaderReset(Loader<RTCData<T>> loader) {
		// Clear the data in the adapter.
		adapter.clear();
	}
	
	/**
	 * 
	 * SEARCH
	 * 
	 */
	// The SearchView for doing filtering.
	private SearchView mSearchView;

	@Override
	public boolean onQueryTextSubmit(String query) {
		RTCClientApplication appState = ((RTCClientApplication) getActivity().getApplicationContext());
		String simpleQueryURL = appState.getServices().getSimpleQuery() + "?oslc_cm.query=oslc_cm:searchTerms=%22%2A" + query + "%2A%22&";
		appState.setQueryURL(simpleQueryURL);

		Intent intent = new Intent(this.getActivity(), ResultQueryActivity.class);
		startActivity(intent);
		return true;
	}

	@Override
	public boolean onQueryTextChange(String arg0) {
		return false;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		// Place an action bar item for searching.
		MenuItem item = menu.add("Search");
		item.setIcon(android.R.drawable.ic_menu_search);
		item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		mSearchView = new MySearchView(getActivity());
		mSearchView.setOnQueryTextListener(this);
		mSearchView.setIconifiedByDefault(true);
		item.setActionView(mSearchView);

		// Applies white color on searchview text
		int id = mSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
		TextView textView = (TextView) mSearchView.findViewById(id);
		textView.setTextColor(Color.WHITE);
	}

	public static class MySearchView extends SearchView {
		public MySearchView(Context context) {
			super(context);
		}

		// The normal SearchView doesn't clear its search text when
		// collapsed, so we will do this for it.
		@Override
		public void onActionViewCollapsed() {
			setQuery("", false);
			// super.onActionViewCollapsed();
		}
	}
}
