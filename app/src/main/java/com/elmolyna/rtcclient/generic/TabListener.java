package com.elmolyna.rtcclient.generic;

import android.app.ActionBar;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.app.ActionBar.Tab;
import android.os.Bundle;

public class TabListener<T extends Fragment> implements ActionBar.TabListener {
	private Fragment mFragment;
	private final FragmentActivity mActivity;
	private final String mTag;
	private final Class<T> mClass;
	private final int containerViewId;
	private final Bundle bundle;
	
	/**
	 * Constructor used each time a new tab is created.
	 * 
	 * @param activity
	 *            The host Activity, used to instantiate the fragment
	 * @param containerViewId
	 *            The container to replace
	 * @param tag
	 *            The identifier tag for the fragment
	 * @param clz
	 *            The fragment's Class, used to instantiate the fragment
	 */
	public TabListener(Activity activity, int containerViewId, String tag, Class<T> clz, Bundle bundle) {
		mActivity = (FragmentActivity)activity;
		mTag = tag;
		mClass = clz;
		this.containerViewId = containerViewId;
		this.bundle = bundle;
	}

	/* The following are each of the ActionBar.TabListener callbacks */
	@Override
	public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {

		
		// Check if the fragment is already initialized
		if (mFragment == null) {
			FragmentTransaction fft = mActivity.getSupportFragmentManager().beginTransaction();
			/*
			 * The fragment which has been added to this listener may have been replaced (can be the case for lists when drilling down), but if the tag has been
			 * retained, we should find the actual fragment that was showing in this tab before the user switched to another.
			 */
			Fragment retainFragment = mActivity.getSupportFragmentManager().findFragmentByTag(mTag);
			if (retainFragment != null) {
				mFragment = retainFragment;
				fft.attach(mFragment);
			} else {
				// If not, instantiate and add it to the activity
				mFragment = Fragment.instantiate(mActivity, mClass.getName());
				mFragment.setArguments(bundle);
				fft.replace(this.containerViewId, mFragment, mTag);
			}
			fft.commit();
		} else {
			FragmentTransaction fft = mActivity.getSupportFragmentManager().beginTransaction();
			// If it exists, simply attach it in order to show it
			fft.attach(mFragment);
			fft.commit();
		}
		
	}
	
	@Override
	public void onTabUnselected(Tab tab, android.app.FragmentTransaction ft) {
		
		if (mFragment == null) {
			
			/*
			 * The fragment which has been added to this listener may have been replaced (can be the case for lists when drilling down), but if the tag has been
			 * retained, we should find the actual fragment that's currently active.
			 */
			Fragment retainFragment = mActivity.getSupportFragmentManager().findFragmentByTag(mTag);
			if (retainFragment != null) {
				FragmentTransaction fft = mActivity.getSupportFragmentManager().beginTransaction();
				mFragment = retainFragment;
				fft.detach(mFragment);
				fft.commit();
			}
		} else {
			FragmentTransaction fft = mActivity.getSupportFragmentManager().beginTransaction();
			// Detach the fragment, because another one is being attached
			fft.detach(mFragment);
			fft.commit();
		}
	}

	@Override
	public void onTabReselected(Tab tab, android.app.FragmentTransaction ft) {
		
	}
	
}
