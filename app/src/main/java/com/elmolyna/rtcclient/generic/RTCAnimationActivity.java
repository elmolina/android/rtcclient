package com.elmolyna.rtcclient.generic;

import android.os.Bundle;

import com.elmolyna.rtcclient.R;

public class RTCAnimationActivity extends RTCActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.pull_in_from_right, R.anim.hold);
	}
}
