package com.elmolyna.rtcclient;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;

import com.elmolyna.rtcclient.fragments.QueriesTabFragment;

import com.elmolyna.rtcclient.generic.RTCAnimationActivity;

public class QueriesActivity extends RTCAnimationActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		setContentView(R.layout.queries_fragment);
		// Show the Up button in the action bar.

		if (savedInstanceState == null) {
			FragmentManager fm = getSupportFragmentManager();
			QueriesTabFragment fragment = new QueriesTabFragment();
			FragmentTransaction transaction = fm.beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			//Si seteo el ID no se muestra la botonera
			transaction.add(R.id.queries_tab_fragment, fragment, "queriesTabFragment");
			transaction.commit();
		}
	}
}
