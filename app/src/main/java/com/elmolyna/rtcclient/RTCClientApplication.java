package com.elmolyna.rtcclient;

import org.apache.http.client.HttpClient;

import com.elmolyna.rtcclient.conn.HttpUtils;
import com.elmolyna.rtcclient.data.ProjectAreaServicesEntry;

import android.app.Application;

public class RTCClientApplication extends Application {
	
	private HttpClient httpClient = HttpUtils.getNewHttpClient();

	private String connection_id = null;
	private String username = null;
	private String password = null;
	private String connectionUrl = null;
	private String projectAreaDetail = null;
	private String projectAreaServices = null;
	
	public int getWorkitemTabFragmentSelectedTabIndex() {
		return workitemTabFragmentSelectedTabIndex;
	}

	public void setWorkitemTabFragmentSelectedTabIndex(int workitemTabFragmentSelectedTabIndex) {
		this.workitemTabFragmentSelectedTabIndex = workitemTabFragmentSelectedTabIndex;
	}

	private String queryURL = null;
	private String workitemURL = null;
	private ProjectAreaServicesEntry services = null;
	
	private int queriesTabFragmentSelectedTabIndex = 0;
	private int workitemTabFragmentSelectedTabIndex = 0;
	
	public HttpClient getHttpClient() {
		return httpClient;
	}
	
	public void restartHttpClient() {
		httpClient = HttpUtils.getNewHttpClient();
	}
	
	public String getConnection_id() {
		return connection_id;
	}

	public void setConnection_id(String connection_id) {
		this.connection_id = connection_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConnectionUrl() {
		return connectionUrl;
	}

	public void setConnectioUrl(String connectionUrl) {
		this.connectionUrl = connectionUrl;
	}

	public String getProjectAreaDetail() {
		return projectAreaDetail;
	}

	public void setProjectAreaDetail(String projectAreaDetail) {
		this.projectAreaDetail = projectAreaDetail;
	}

	public String getProjectAreaServices() {
		return projectAreaServices;
	}

	public void setProjectAreaServices(String projectAreaServices) {
		this.projectAreaServices = projectAreaServices;
	}

	public ProjectAreaServicesEntry getServices() {
		return services;
	}

	public void setServices(ProjectAreaServicesEntry services) {
		this.services = services;
	}

	public String getQueryURL() {
		return queryURL;
	}

	public void setQueryURL(String queryURL) {
		this.queryURL = queryURL;
	}

	public int getQueriesTabFragmentSelectedTabIndex() {
		return queriesTabFragmentSelectedTabIndex;
	}

	public void setQueriesTabFragmentSelectedTabIndex(int queriesTabFragmentSelectedTabIndex) {
		this.queriesTabFragmentSelectedTabIndex = queriesTabFragmentSelectedTabIndex;
	}

	public String getWorkitemURL() {
		return workitemURL;
	}

	public void setWorkitemURL(String workitemURL) {
		this.workitemURL = workitemURL;
	}
}
