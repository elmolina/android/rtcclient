package com.elmolyna.rtcclient.data;

public class ResultQueryEntry {

	private StringBuffer identifier = new StringBuffer();
	private String url = null;
	private StringBuffer title = new StringBuffer();
	private StringBuffer filedAgainst = new StringBuffer();
	private StringBuffer plannedFor = new StringBuffer();
	private boolean hasParent = false;
	private String parentUrl = null;
	private StringBuffer typeIconUrl = new StringBuffer();

	private boolean hasChildrem = false;
	private String childrenUrl = null;
	private int childrenNumber = 0;
	
	public ResultQueryEntry() {
	};

	public String getIdentifier() {
		return identifier.toString();
	}

	public String getTitle() {
		return title.toString();
	}

	public void addIdentifier(String identifier) {
		this.identifier.append(identifier);
	}

	public void addTitle(String title) {
		this.title.append(title);
	}

	public String getFiledAgainst() {
		return filedAgainst.toString();
	}

	public void addFiledAgainst(String filedAgainst) {
		this.filedAgainst.append(filedAgainst);
	}

	public String getPlannedFor() {
		return plannedFor.toString();
	}

	public void addPlannedFor(String plannedFor) {
		this.plannedFor.append(plannedFor);
	}

	public String getTypeIconUrl() {
		return typeIconUrl.toString();
	}

	public void addTypeIconUrl(String typeIconUrl) {
		this.typeIconUrl.append(typeIconUrl);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getParentUrl() {
		return parentUrl;
	}

	public void setParentUrl(String parentUrl) {
		this.parentUrl = parentUrl;
	}

	public boolean isHasParent() {
		return hasParent;
	}

	public void setHasParent(boolean hasParent) {
		this.hasParent = hasParent;
	}

	public boolean isHasChildrem() {
		return hasChildrem;
	}

	public void setHasChildrem(boolean hasChildrem) {
		this.hasChildrem = hasChildrem;
	}

	public String getChildrenUrl() {
		return childrenUrl;
	}

	public void setChildrenUrl(String childrenUrl) {
		this.childrenUrl = childrenUrl;
	}

	public int getChildrenNumber() {
		return childrenNumber;
	}

	public void setChildrenNumber(int childrenNumber) {
		this.childrenNumber = childrenNumber;
	}
	
	public void addChildrenNumber() {
		this.childrenNumber++;
	}
}
