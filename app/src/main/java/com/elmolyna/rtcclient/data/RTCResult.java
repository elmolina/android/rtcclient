package com.elmolyna.rtcclient.data;

public class RTCResult {

	public static final int OK = 0;
	public static final int PROTOCOL_ERROR = 1;
	public static final int CREDENTIAL_ERROR = 2;
	public static final int IO_ERROR = 3;
	public static final int GENERIC_ERROR = 4;
	public static final int REPOSITORY_ERROR = 5;
	
	private int result = GENERIC_ERROR;
	private String message = null;

	public RTCResult(int result) {
		this.result = result;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
