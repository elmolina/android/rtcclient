package com.elmolyna.rtcclient.data;

public class WorkItemCommentEntry {

	private StringBuffer created = new StringBuffer();
	private StringBuffer creator = new StringBuffer();
	private StringBuffer description = new StringBuffer();
	
	public WorkItemCommentEntry() {
	}

	public String getDescription() {
		return description.toString();
	}

	public void addDescription(String description) {
		this.description.append(description);
	}

	public String getCreated() {
		return created.toString();
	}

	public void addCreated(String created) {
		this.created.append(created);
	}

	
	public String getCreator() {
		return creator.toString();
	}

	public void addCreator(String creator) {
		this.creator.append(creator);
	}

	
}