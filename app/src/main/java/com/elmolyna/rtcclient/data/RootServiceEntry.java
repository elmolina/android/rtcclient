package com.elmolyna.rtcclient.data;

public class RootServiceEntry {

	private String serviceProvider = null;

	public RootServiceEntry() {
	}

	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	};

}
