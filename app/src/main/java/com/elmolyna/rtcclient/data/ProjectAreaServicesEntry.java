package com.elmolyna.rtcclient.data;

public class ProjectAreaServicesEntry {

	private StringBuffer simpleQuery = new StringBuffer();
	
	public ProjectAreaServicesEntry() {
	}

	public String getSimpleQuery() {
		return simpleQuery.toString();
	}

	public void addSimpleQuery(String simpleQuery) {
		this.simpleQuery.append(simpleQuery);
	}
}


