package com.elmolyna.rtcclient.data;

import java.util.List;



public class RTCData <D>{
	
	private List<D> list = null;
	private String next = null;
	private String previous = null;
	private int result = RTCResult.GENERIC_ERROR;
	private String message = null;
	private int title;
	
	public RTCData(){
		
	}
	
	public RTCData(List<D> list, int result, int title, String message){
		this.list = list;
		this.result = result;
		this.title = title;
		this.message = message;		
	}
	
	public List<D> getList() {
		return list;
	}
	
	public int getResult() {
		return result;
	}
	
	public void setList(List<D> list) {
		this.list = list;
	}
	
	public void setResult(int result) {
		this.result = result;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public int getTitle() {
		return title;
	}

	public void setTitle(int title) {
		this.title = title;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public String getPrevious() {
		return previous;
	}

	public void setPrevious(String previous) {
		this.previous = previous;
	}
}
