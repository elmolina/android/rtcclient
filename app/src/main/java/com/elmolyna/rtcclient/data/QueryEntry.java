package com.elmolyna.rtcclient.data;

public class QueryEntry {

	private StringBuffer title = new StringBuffer();
	private String result = null;

	public QueryEntry() {
	}

	public QueryEntry(String title, String result) {
		this.title.append(title);
		this.result = result;
	}

	public String getTitle() {
		return title.toString();
	}

	public void addTitle(String title) {
		this.title.append(title);
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
