package com.elmolyna.rtcclient.data;

public class ProjectAreaDetailEntry {

	private StringBuffer description = new StringBuffer();
	
	public ProjectAreaDetailEntry() {
	}

	public String getDescription() {
		return description.toString();
	}

	public void addDescription(String description) {
		this.description.append(description);
	}
}