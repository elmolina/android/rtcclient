package com.elmolyna.rtcclient.data;

public class ProjectAreaEntry {

	private StringBuffer title = new StringBuffer();
	private String detailUrl = null;
	private String service = null;

	public ProjectAreaEntry() {
	}

	public String getTitle() {
		return title.toString();
	}

	public void addTitle(String title) {
		this.title.append(title);
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getDetailUrl() {
		return detailUrl;
	}

	public void setDetailUrl(String detailUrl) {
		this.detailUrl = detailUrl;
	}
}
