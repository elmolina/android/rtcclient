package com.elmolyna.rtcclient.data;

public class WorkItemEntry {

	private StringBuffer identifier = new StringBuffer();
	private String workitemURL = null;
	private StringBuffer title = new StringBuffer();
	private StringBuffer filedAgainst = new StringBuffer();
	private StringBuffer plannedFor = new StringBuffer();
	private StringBuffer typeTitle = new StringBuffer();
	private StringBuffer typeIconUrl = new StringBuffer();
	private StringBuffer priorityTitle = new StringBuffer();
	private StringBuffer priorityIconUrl = new StringBuffer();
	private StringBuffer severityTitle = new StringBuffer();
	private StringBuffer severityIconUrl = new StringBuffer();
	private StringBuffer stateTitle = new StringBuffer();
	private StringBuffer stateIconUrl = new StringBuffer();
	private String parent = null;
	private StringBuffer description = new StringBuffer();
	private boolean hasParent = false;

	public boolean isHasParent() {
		return hasParent;
	}

	public WorkItemEntry() {
	};

	public String getIdentifier() {
		return identifier.toString();
	}

	public String getTitle() {
		return title.toString();
	}

	public void addIdentifier(String identifier) {
		this.identifier.append(identifier);
	}

	public void addTitle(String title) {
		this.title.append(title);
	}

	public String getFiledAgainst() {
		return filedAgainst.toString();
	}

	public void addFiledAgainst(String filedAgainst) {
		this.filedAgainst.append(filedAgainst);
	}

	public String getPlannedFor() {
		return plannedFor.toString();
	}

	public void addPlannedFor(String plannedFor) {
		this.plannedFor.append(plannedFor);
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public boolean hasParent() {
		return hasParent;
	}

	public void setHasParent(boolean hasParent) {
		this.hasParent = hasParent;
	}

	public String getWorkitemURL() {
		return workitemURL;
	}

	public void setWorkitemURL(String workitemURL) {
		this.workitemURL = workitemURL;
	}

	public String getTypeTitle() {
		return typeTitle.toString();
	}

	public void addTypeTitle(String typeTitle) {
		this.typeTitle.append(typeTitle);
	}

	public String getTypeIconUrl() {
		return typeIconUrl.toString();
	}

	public void addTypeIconUrl(String typeIconUrl) {
		this.typeIconUrl.append(typeIconUrl);
	}

	public String getPriorityTitle() {
		return priorityTitle.toString();
	}

	public void addPriorityTitle(String priorityTitle) {
		this.priorityTitle.append(priorityTitle);
	}

	public String getPriorityIconUrl() {
		return priorityIconUrl.toString();
	}

	public void addPriorityIconUrl(String priorityIconUrl) {
		this.priorityIconUrl.append(priorityIconUrl);
	}

	public String getStateTitle() {
		return stateTitle.toString();
	}

	public void addStateTitle(String stateTitle) {
		this.stateTitle.append(stateTitle);
	}

	public String getStateIconUrl() {
		return stateIconUrl.toString();
	}

	public void addStateIconUrl(String stateIconUrl) {
		this.stateIconUrl.append(stateIconUrl);
	}

	public String getSeverityTitle() {
		return severityTitle.toString();
	}

	public void addSeverityTitle(String severityTitle) {
		this.severityTitle.append(severityTitle);
	}

	public String getSeverityIconUrl() {
		return severityIconUrl.toString();
	}

	public void addSeverityIconUrl(String severityIconUrl) {
		this.severityIconUrl.append(severityIconUrl);
	}

	public String getDescription() {
		// Las descriptciones vienen con <br/> y hay que substituirlas por saltos de carro
		return description.toString();
	}

	public void addDescription(String description) {
		this.description.append(description);
	}
}
