package com.elmolyna.rtcclient.fragments;

import android.content.Context;
import android.support.v4.content.Loader;
import android.os.Bundle;

import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.data.QueryEntry;
import com.elmolyna.rtcclient.data.RTCData;
import com.elmolyna.rtcclient.generic.RTCLoader;
import com.elmolyna.rtcclient.oslc.OSLCUtils;

public class QueriesAreaFragment extends QueriesFragment {

	private QueriesAreaLoader loader;

	// LoaderManager.LoaderCallbacks
	@Override
	public Loader<RTCData<QueryEntry>> onCreateLoader(int id, Bundle args) {
		// This is called when a new Loader needs to be created. This
		// sample only has one Loader with no arguments, so it is simple.			
		String next = null;
		if (args != null)
			next = args.getString("NEXT");
		loader = new QueriesAreaLoader(getActivity(), next);
		return loader;
	}

	/**
	 * LOADER
	 */
	public static class QueriesAreaLoader extends RTCLoader<QueryEntry> {
		private String next = null;
		
		public QueriesAreaLoader(Context context, String next) {
			super(context);
			this.next = next;
		}
		
		/**
		 * This is where the bulk of our work is done. This function is called in a background thread and should generate a new set of data to be published by
		 * the loader.
		 */
		@Override
		public RTCData<QueryEntry> loadInBackground() {
			// Retrieve user queries
			RTCClientApplication appState = ((RTCClientApplication) this.getContext().getApplicationContext());
			if (next == null)
				return OSLCUtils.getAreaQueries(appState);
			return OSLCUtils.getNextQueries(appState, next);
		}
	}
}
