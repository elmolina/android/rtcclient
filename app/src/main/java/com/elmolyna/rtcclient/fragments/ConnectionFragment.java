package com.elmolyna.rtcclient.fragments;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.elmolyna.rtcclient.generic.ActivityListener;
import com.elmolyna.rtcclient.NewConnectionActivity;
import com.elmolyna.rtcclient.ProjectAreasActivity;
import com.elmolyna.rtcclient.R;
import com.elmolyna.rtcclient.RTCClientApplication;

public class ConnectionFragment extends ListFragment {

	// This is the Adapter being used to display the list's data.
	private ConnectionAdapter adapter;

	// Communication with activity
	private ActivityListener activityListener;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			this.activityListener = (ActivityListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement ActivityListener");
		}
	}

	public void showErrorDialog(int title, String message) {
		activityListener.showErrorDialog(title, message);
	}


	@Override
	public void onResume() {
		super.onResume();
		//Reiniciamos el cliente HTTP
		RTCClientApplication appState = ((RTCClientApplication) getActivity().getApplicationContext());
		appState.restartHttpClient();
	}

	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Give some text to display if there is no data. In a real
		// application this would come from a resource.
		this.setEmptyText(getText(R.string.connection_empty_list));

		// Create an empty adapter we will use to display the loaded data.
		adapter = new ConnectionAdapter(getActivity());
		setListAdapter(adapter);

		SharedPreferences preferencias = getActivity().getSharedPreferences("datos", Context.MODE_PRIVATE);
		Set<String> ids = preferencias.getStringSet("ids", new HashSet<String>());
		
		if (ids.size() == 0) {
			addDefaultConnection();
			ids = preferencias.getStringSet("ids", new HashSet<String>());
		}
		
		adapter.setData(new ArrayList<String>(ids));

		setHasOptionsMenu(true);
		registerForContextMenu(getListView());
	}

	public void addDefaultConnection () {
		String new_connection_id = "Jazz Testing Connection";
		SharedPreferences preferencias =  getActivity().getSharedPreferences("datos", Context.MODE_PRIVATE);
		Editor editor = preferencias.edit();
		Set<String> ids = preferencias.getStringSet("ids", new HashSet<String>());
		
		ids.add(new_connection_id);

		editor.putStringSet("ids", ids);
		editor.putString(new_connection_id + "new_connection_url", "https://jazz.net/jazz/");
		editor.putString(new_connection_id + "new_connection_username", "rtcclienttesting");
		editor.putString(new_connection_id + "new_connection_password", "rtcclienttesting123");
		editor.commit();
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.connection_remove:
			AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
			String entry = (String) this.getListView().getItemAtPosition(info.position);

			SharedPreferences preferencias = getActivity().getSharedPreferences("datos", Context.MODE_PRIVATE);
			Editor editor = preferencias.edit();
			Set<String> ids = preferencias.getStringSet("ids", new HashSet<String>());
			ids.remove(entry);

			editor.putStringSet("ids", ids);
			editor.remove(entry + "new_connection_url");
			editor.remove(entry + "new_connection_username");
			editor.remove(entry + "new_connection_password");
			editor.commit();

			adapter.setData(new ArrayList<String>(ids));

			adapter.notifyDataSetChanged();
			return true;
		default:
			return false;
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getActivity().getMenuInflater().inflate(R.menu.connection_context_menu, menu);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		getActivity().getMenuInflater().inflate(R.menu.connection_options_menu, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.connect_new:
			// app icon in action bar clicked; go home
			Intent intent = new Intent(this.getActivity(), NewConnectionActivity.class);

			startActivity(intent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		String entry = (String) l.getItemAtPosition(position);
		SharedPreferences preferencias = getActivity().getSharedPreferences("datos", Context.MODE_PRIVATE);

		String new_connection_url = preferencias.getString(entry + "new_connection_url", null);
		String new_connection_username = preferencias.getString(entry + "new_connection_username", null);
		String new_connection_password = preferencias.getString(entry + "new_connection_password", null);

		RTCClientApplication appState = ((RTCClientApplication) getActivity().getApplicationContext());
		appState.setConnection_id(entry);
		appState.setConnectioUrl(new_connection_url);
		appState.setUsername(new_connection_username);
		appState.setPassword(new_connection_password);

		Intent intent = new Intent(this.getActivity(), ProjectAreasActivity.class);
		startActivity(intent);
	}

	/**
	 * ADAPTER
	 */

	public static class ConnectionAdapter extends ArrayAdapter<String> {
		private final LayoutInflater mInflater;

		public ConnectionAdapter(Context context) {
			super(context, android.R.layout.simple_list_item_2);
			mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public void setData(List<String> data) {
			clear();
			if (data != null) {
				addAll(data);
			}
		}

		/**
		 * Populate new items in the list.
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view;

			if (convertView == null) {
				// Infla el layout correspondiente a un elemento de la lista
				view = mInflater.inflate(R.layout.connection_entry, parent, false);
			} else {
				view = convertView;
			}

			String entry = getItem(position);
			((TextView) view.findViewById(R.id.connection_entry_id)).setText(entry);

			return view;
		}
	}
}
