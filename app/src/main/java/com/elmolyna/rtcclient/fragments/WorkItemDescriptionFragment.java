package com.elmolyna.rtcclient.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elmolyna.rtcclient.R;
import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.data.RTCData;
import com.elmolyna.rtcclient.data.RTCResult;
import com.elmolyna.rtcclient.data.WorkItemEntry;
import com.elmolyna.rtcclient.generic.RTCAsyncFragment;
import com.elmolyna.rtcclient.generic.RTCLoader;
import com.elmolyna.rtcclient.oslc.OSLCUtils;

public class WorkItemDescriptionFragment extends RTCAsyncFragment implements LoaderManager.LoaderCallbacks<RTCData<WorkItemEntry>> {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.workitem_description_fragment, container, false);
		return super.onCreateAsyncView(view, android.R.attr.progressBarStyleLarge);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		// Prepare the loader. Either re-connect with an existing one, or start a new one.
		getLoaderManager().initLoader(0, null, this);
	}

	// LoaderManager.LoaderCallbacks
	@Override
	public Loader<RTCData<WorkItemEntry>> onCreateLoader(int id, Bundle args) {
		// This is called when a new Loader needs to be created. This
		// sample only has one Loader with no arguments, so it is simple.
		return new WorkItemLoader(getActivity());
	}

	// LoaderManager.LoaderCallbacks
	@Override
	public void onLoadFinished(Loader<RTCData<WorkItemEntry>> loader, RTCData<WorkItemEntry> data) {

		if (data.getResult() != RTCResult.OK) {
			super.showErrorDialog(data.getTitle(), data.getMessage());
		} else {
			WorkItemEntry entry = data.getList().get(0);
			((TextView) getView().findViewById(R.id.workitem_description)).setText(Html.fromHtml(entry.getDescription()));
		}
		//mostramos el resultado
		super.setContentShown(true);
	}

	@Override
	public void onLoaderReset(Loader<RTCData<WorkItemEntry>> arg0) {
	}

	/**
	 * LOADER
	 */
	public static class WorkItemLoader extends RTCLoader<WorkItemEntry> {

		public WorkItemLoader(Context context) {
			super(context);
		}

		/**
		 * This is where the bulk of our work is done. This function is called in a background thread and should generate a new set of data to be published by
		 * the loader.
		 */
		@Override
		public RTCData<WorkItemEntry> loadInBackground() {
			// Retrieve user queries
			RTCClientApplication appState = ((RTCClientApplication) this.getContext().getApplicationContext());

			RTCData<WorkItemEntry> data = OSLCUtils.getWorkItemDescription(appState);
			if (data.getResult() == RTCResult.OK && data.getList().size() != 1) {
				data.setResult(RTCResult.REPOSITORY_ERROR);
				data.setTitle(R.string.new_connection_dialog_message_repository_error);
			}
			return data;
		}
	}
}
