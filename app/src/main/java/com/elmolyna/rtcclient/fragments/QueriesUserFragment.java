package com.elmolyna.rtcclient.fragments;

import android.content.Context;
import android.support.v4.content.Loader;
import android.os.Bundle;

import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.data.QueryEntry;
import com.elmolyna.rtcclient.data.RTCData;
import com.elmolyna.rtcclient.generic.RTCLoader;
import com.elmolyna.rtcclient.oslc.OSLCUtils;

public class QueriesUserFragment extends QueriesFragment {

	private QueriesUserLoader loader;

	// LoaderManager.LoaderCallbacks
	@Override
	public Loader<RTCData<QueryEntry>> onCreateLoader(int id, Bundle args) {
		// This is called when a new Loader needs to be created. This
		// sample only has one Loader with no arguments, so it is simple.
		loader = new QueriesUserLoader(getActivity());
		return loader;
	}

	/**
	 * LOADER
	 */
	public static class QueriesUserLoader extends RTCLoader<QueryEntry> {

		public QueriesUserLoader(Context context) {
			super(context);
		}

		/**
		 * This is where the bulk of our work is done. This function is called in a background thread and should generate a new set of data to be published by
		 * the loader.
		 */
		@Override
		public RTCData<QueryEntry> loadInBackground() {
			// Retrieve user queries
			RTCClientApplication appState = ((RTCClientApplication) this.getContext().getApplicationContext());
			return OSLCUtils.getUserQueries(appState);
		}
	}
}
