package com.elmolyna.rtcclient.fragments;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import android.content.Context;
import android.support.v4.content.Loader;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;

import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.data.QueryEntry;
import com.elmolyna.rtcclient.data.RTCData;
import com.elmolyna.rtcclient.data.RTCResult;
import com.elmolyna.rtcclient.generic.RTCLoader;

public class QueriesFavoritesFragment extends QueriesFragment {
	
	private QueriesFavoritesLoader loader;

	// LoaderManager.LoaderCallbacks
	@Override
	public Loader<RTCData<QueryEntry>> onCreateLoader(int id, Bundle args) {
		// This is called when a new Loader needs to be created. This
		// sample only has one Loader with no arguments, so it is simple.
		loader = new QueriesFavoritesLoader(getActivity());
		return loader;
	}

	/**
	 * LOADER
	 */
	public static class QueriesFavoritesLoader extends RTCLoader<QueryEntry> {
	
		public QueriesFavoritesLoader(Context context) {
			super(context);
		}

		/**
		 * This is where the bulk of our work is done. This function is called in a background thread and should generate a new set of data to be published by
		 * the loader.
		 */
		@Override
		public RTCData<QueryEntry> loadInBackground() {
			RTCClientApplication appState = ((RTCClientApplication) this.getContext().getApplicationContext());
			String base64ProjectAreaId = Base64.encodeToString(appState.getProjectAreaDetail().getBytes(), Base64.DEFAULT);
	     	SharedPreferences preferencias = getContext().getSharedPreferences("favoriteQueries_" + base64ProjectAreaId, Context.MODE_PRIVATE);
			Map<String, ?> favoriteMap = preferencias.getAll();
			Iterator<String> it = favoriteMap.keySet().iterator();
			
			ArrayList<QueryEntry> list = new ArrayList<QueryEntry>();
			
			while (it.hasNext()) {
				String result = it.next();
				String title = preferencias.getString(result, null);
				QueryEntry entry = new QueryEntry(title, result);
				list.add(entry);
			}
			RTCData<QueryEntry> data =  new RTCData<QueryEntry>();
			data.setList(list);
			data.setResult(RTCResult.OK);
			
			return data;			
		}
	}
}
