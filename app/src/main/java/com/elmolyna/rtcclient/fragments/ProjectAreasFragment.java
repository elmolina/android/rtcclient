package com.elmolyna.rtcclient.fragments;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.elmolyna.rtcclient.QueriesActivity;
import com.elmolyna.rtcclient.R;
import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.data.ProjectAreaEntry;
import com.elmolyna.rtcclient.data.RTCData;
import com.elmolyna.rtcclient.data.RTCResult;
import com.elmolyna.rtcclient.data.RootServiceEntry;
import com.elmolyna.rtcclient.generic.RTCListFragment;
import com.elmolyna.rtcclient.generic.RTCLoader;
import com.elmolyna.rtcclient.oslc.OSLCUtils;

public class ProjectAreasFragment extends RTCListFragment<ProjectAreaEntry> {

	public final static String PROJECT_AREA_ID = "PROJECT_AREA_ID";
	public final static String PROJECT_AREA_SERVICE = "PROJECT_AREA_SERVICE";

	private ProjectAreasLoader loader;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Give some text to display if there is no data. In a real
		// application this would come from a resource.
		this.setEmptyText(getText(R.string.project_areas_empty_list));

		// Create an empty adapter we will use to display the loaded data.
		adapter = new ProjectAreasAdapter(getActivity());
		setListAdapter(adapter);

		// Start out with a progress indicator.
		setListShown(false);

		// Prepare the loader. Either re-connect with an existing one, or start a new one.
		getLoaderManager().restartLoader(0, null, this);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		RTCClientApplication appState = ((RTCClientApplication) this.getActivity().getApplicationContext());
		appState.setProjectAreaDetail(loader.getData().getList().get(position).getDetailUrl());
		appState.setProjectAreaServices(loader.getData().getList().get(position).getService());
		// Inicializamos el Índice del tabulador
		appState.setQueriesTabFragmentSelectedTabIndex(0);
		Intent intent = new Intent(this.getActivity(), QueriesActivity.class);
		startActivity(intent);
	}

	// LoaderManager.LoaderCallbacks
	@Override
	public Loader<RTCData<ProjectAreaEntry>> onCreateLoader(int id, Bundle args) {
		// This is called when a new Loader needs to be created. This
		// sample only has one Loader with no arguments, so it is simple.
		loader = new ProjectAreasLoader(getActivity());
		return loader;
	}

	/**
	 * ADAPTER
	 */

	public class ProjectAreasAdapter extends ArrayAdapter<ProjectAreaEntry> {
		private final LayoutInflater mInflater;

		public ProjectAreasAdapter(Context context) {
			super(context, R.layout.project_area_entry);
			mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		public void setData(List<ProjectAreaEntry> data) {
			clear();
			if (data != null) {
				addAll(data);
			}
		}

		/**
		 * Populate new items in the list.
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view;

			if (convertView == null) {
				// Infla el layout correspondiente a un elemento de la lista
				view = mInflater.inflate(R.layout.project_area_entry, parent, false);
			} else {
				view = convertView;
			}

			ProjectAreaEntry item = getItem(position);
			((TextView) view.findViewById(R.id.project_areas_entry_title)).setText(item.getTitle());

			ImageView favicon = (ImageView) view.findViewById(R.id.project_areas_detail_icon);
			favicon.setOnTouchListener(new ProjectAreaDetailListener(view, item, position));

			return view;
		}
	}

	public class ProjectAreaDetailListener implements OnTouchListener {

		private ProjectAreaEntry item;
		private int position;
		private View view;

		public ProjectAreaDetailListener(View view, ProjectAreaEntry item, int position) {
			this.item = item;
			this.position = position;
			this.view = view;
		}

		@Override
		public boolean onTouch(View buttonView, MotionEvent event) {
			View frameLayout = null;
			switch (event.getAction()) {
			case MotionEvent.ACTION_UP:
				// set a random id to aboid duplicates and couse we are in a list
				int id = position + 123;

				frameLayout = view.findViewById(R.id.project_areas_fragment_container);
				if (frameLayout == null) {
					// En caso que no exista el frame original, es porque se ha substituido con el fragmento de la transacción
					frameLayout = view.findViewById(id);
				}

				// we get the 'childFragmentManager' for our transaction because we want a child
				FragmentManager fm = getChildFragmentManager();

				if (frameLayout.getVisibility() == View.VISIBLE) {
					frameLayout.setVisibility(View.GONE);
					((ImageView) buttonView).setImageResource(R.drawable.btn_plus_default);

					// remove the previos fragment to avoid visibility issues when reshown
					ProjectAreaDetailFragment fragment = (ProjectAreaDetailFragment) fm.findFragmentById(id);
					FragmentTransaction transaction = fm.beginTransaction();
					if (fragment != null) {
						transaction.remove(fragment);
						transaction.commit();
					}
				} else {
					((ImageView) buttonView).setImageResource(R.drawable.btn_plus_selected);

					FragmentTransaction transaction = fm.beginTransaction();
					ProjectAreaDetailFragment fragment = new ProjectAreaDetailFragment();
					// make the back button return to the main screen
					// and supply the tag 'left' to the backstack
					Bundle bundle = new Bundle();
					bundle.putString("DETAIL", item.getDetailUrl());
					fragment.setArguments(bundle);

					// seteamos un id diferente para cada entrada de la lista porque si no siempre sustituye la primera entrada
					frameLayout.setId(id);
					// Hay que hacer un replace para que el nested fragment funcione
					transaction.replace(id, fragment);

					// commit the transaction
					transaction.commit();
					frameLayout.setVisibility(View.VISIBLE);
				}
				return true;
			case MotionEvent.ACTION_DOWN:
				((ImageView) buttonView).setImageResource(R.drawable.btn_plus_pressed);
				return true;

			default:
				return false;
			}
		}
	}

	/**
	 * LOADER
	 */
	public static class ProjectAreasLoader extends RTCLoader<ProjectAreaEntry> {

		public ProjectAreasLoader(Context context) {
			super(context);
		}

		/**
		 * This is where the bulk of our work is done. This function is called in a background thread and should generate a new set of data to be published by
		 * the loader.
		 */
		@Override
		public RTCData<ProjectAreaEntry> loadInBackground() {
			// Retrieve user queries
			RTCClientApplication appState = ((RTCClientApplication) this.getContext().getApplicationContext());
			RTCData<RootServiceEntry> rootServiceData = OSLCUtils.getServiceProvider(appState);
			RTCData<ProjectAreaEntry> projectAreaData = new RTCData<ProjectAreaEntry>();

			if (rootServiceData.getResult() == RTCResult.OK) {
				if (rootServiceData.getList().size() == 1) {
					String serviceProvider = rootServiceData.getList().get(0).getServiceProvider();
					projectAreaData = OSLCUtils.getProjectAreas(appState, serviceProvider);
				} else {
					projectAreaData.setResult(RTCResult.REPOSITORY_ERROR);
					projectAreaData.setTitle(R.string.new_connection_dialog_message_repository_error);
				}
			} else {
				projectAreaData.setResult(rootServiceData.getResult());
				projectAreaData.setTitle(rootServiceData.getTitle());
				projectAreaData.setMessage(rootServiceData.getMessage());
			}

			return projectAreaData;
		}
	}
}
