package com.elmolyna.rtcclient.fragments;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elmolyna.rtcclient.R;
import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.generic.RTCFragment;
import com.elmolyna.rtcclient.generic.TabListener;

public class WorkItemTabFragment extends RTCFragment {
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// We have a menu item to show in action bar.
		setHasOptionsMenu(true);
		
		// setup action bar for tabs
		ActionBar actionBar = getActivity().getActionBar();

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		//actionBar.setDisplayShowTitleEnabled(false);

		Tab tab = actionBar.newTab().setText(R.string.workitem_tab_detail)
				.setTabListener(new TabListener<WorkItemDetailFragment>(this.getActivity(), R.id.workitem_fragment, "workItemDetailFragment", WorkItemDetailFragment.class, getArguments()));
		actionBar.addTab(tab);

		Tab tab2 = actionBar.newTab().setText(R.string.workitem_tab_description)
				.setTabListener(new TabListener<WorkItemDescriptionFragment>(this.getActivity(), R.id.workitem_fragment, "workItemDescriptionFragment", WorkItemDescriptionFragment.class, getArguments()));
		actionBar.addTab(tab2);
		
		Tab tab3 = actionBar.newTab().setText(R.string.workitem_tab_comments)
				.setTabListener(new TabListener<WorkItemCommentsFragment>(this.getActivity(), R.id.workitem_fragment, "WorkItemCommentsFragment", WorkItemCommentsFragment.class, getArguments()));
		actionBar.addTab(tab3);
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		// Save the index of the currently selected tab

		int index = getActivity().getActionBar().getSelectedTab().getPosition();
		RTCClientApplication appState = ((RTCClientApplication) getActivity().getApplicationContext());
		appState.setWorkitemTabFragmentSelectedTabIndex(index);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		RTCClientApplication appState = ((RTCClientApplication) getActivity().getApplicationContext());
		int index = appState.getWorkitemTabFragmentSelectedTabIndex();
		getActivity().getActionBar().setSelectedNavigationItem(index);
		return super.onCreateView(inflater, container, savedInstanceState);
	}
}