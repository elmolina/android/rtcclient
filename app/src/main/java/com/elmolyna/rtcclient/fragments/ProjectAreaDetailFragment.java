package com.elmolyna.rtcclient.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elmolyna.rtcclient.R;
import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.data.ProjectAreaDetailEntry;
import com.elmolyna.rtcclient.data.RTCData;
import com.elmolyna.rtcclient.data.RTCResult;
import com.elmolyna.rtcclient.generic.RTCAsyncFragment;
import com.elmolyna.rtcclient.generic.RTCLoader;
import com.elmolyna.rtcclient.oslc.OSLCUtils;

public class ProjectAreaDetailFragment extends RTCAsyncFragment implements LoaderManager.LoaderCallbacks<RTCData<ProjectAreaDetailEntry>> {

	private String detailUrl = null;
	private ProjectAreaDetailLoader loader;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {		
		View view = inflater.inflate(R.layout.project_area_detail_fragment, container, false);
		return super.onCreateAsyncView(view);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	
		Bundle bundle = getArguments();
		detailUrl = bundle.getString("DETAIL");
		// Prepare the loader. Either re-connect with an existing one, or start a new one.
		getLoaderManager().initLoader(0, null, this);
	}

	// LoaderManager.LoaderCallbacks
	@Override
	public Loader<RTCData<ProjectAreaDetailEntry>> onCreateLoader(int id, Bundle args) {
		// This is called when a new Loader needs to be created. This
		// sample only has one Loader with no arguments, so it is simple.
		loader = new ProjectAreaDetailLoader(getActivity(), detailUrl);
		return loader;
	}

	// LoaderManager.LoaderCallbacks
	@Override
	public void onLoadFinished(Loader<RTCData<ProjectAreaDetailEntry>> loader, RTCData<ProjectAreaDetailEntry> data) {

		if (data.getResult() != RTCResult.OK) {
			super.showErrorDialog(data.getTitle(), data.getMessage());
		} else {
			ProjectAreaDetailEntry entry = data.getList().get(0);


			TextView typeTitle = (TextView) getView().findViewById(R.id.project_area_detail_description);
			if (typeTitle != null) {
				if (entry.getDescription() != null && !entry.getDescription().equals(""))
					typeTitle.setText(entry.getDescription());
				else {
					typeTitle.setText(getText(R.string.project_area_detail_empty));		
				}
			}
		}	
		//mostramos el resultado
		super.setContentShown(true);
	}

	@Override
	public void onLoaderReset(Loader<RTCData<ProjectAreaDetailEntry>> arg0) {
	}

	/**
	 * LOADER
	 */
	public static class ProjectAreaDetailLoader extends RTCLoader<ProjectAreaDetailEntry> {

		private String detailUrl = null;

		public ProjectAreaDetailLoader(Context context, String detailUrl) {
			super(context);
			this.detailUrl = detailUrl;
		}

		/**
		 * This is where the bulk of our work is done. This function is called in a background thread and should generate a new set of data to be published by
		 * the loader.
		 */
		@Override
		public RTCData<ProjectAreaDetailEntry> loadInBackground() {
			// Retrieve user queries
			RTCClientApplication appState = ((RTCClientApplication) this.getContext().getApplicationContext());

			RTCData<ProjectAreaDetailEntry> data = OSLCUtils.getProjectAreaDetail(appState,	detailUrl);
			if (data.getResult() == RTCResult.OK && data.getList().size() != 1) {
				data.setResult(RTCResult.REPOSITORY_ERROR);
				data.setTitle(R.string.new_connection_dialog_message_repository_error);
			}
			return data;
		}
	}
}
