package com.elmolyna.rtcclient.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.elmolyna.rtcclient.R;
import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.data.RTCData;
import com.elmolyna.rtcclient.data.RTCResult;
import com.elmolyna.rtcclient.data.WorkItemEntry;
import com.elmolyna.rtcclient.generic.RTCAsyncFragment;
import com.elmolyna.rtcclient.generic.RTCLoader;
import com.elmolyna.rtcclient.oslc.OSLCUtils;
import com.elmolyna.rtcclient.urlimageviewhelper.UrlImageViewHelper;

public class WorkItemDetailFragment extends RTCAsyncFragment implements LoaderManager.LoaderCallbacks<RTCData<WorkItemEntry>> {

	private WorkItemLoader loader;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.workitem_detail_fragment, container, false);
		return super.onCreateAsyncView(view, android.R.attr.progressBarStyleLarge);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Prepare the loader. Either re-connect with an existing one, or start a new one.
		getLoaderManager().initLoader(0, null, this);
	}

	// LoaderManager.LoaderCallbacks
	@Override
	public Loader<RTCData<WorkItemEntry>> onCreateLoader(int id, Bundle args) {
		// This is called when a new Loader needs to be created. This
		// sample only has one Loader with no arguments, so it is simple.
		
		loader = new WorkItemLoader(getActivity());
		return loader;
	}

	// LoaderManager.LoaderCallbacks
	@Override
	public void onLoadFinished(Loader<RTCData<WorkItemEntry>> loader, RTCData<WorkItemEntry> data) {

		if (data.getResult() != RTCResult.OK) {
			super.showErrorDialog(data.getTitle(), data.getMessage());
		} else {
			WorkItemEntry entry = data.getList().get(0);

			ImageView typeIconUrl = (ImageView) getView().findViewById(R.id.workitem_type_icon);
			UrlImageViewHelper.setUrlDrawable(typeIconUrl, entry.getTypeIconUrl());

			TextView typeTitle = (TextView) getView().findViewById(R.id.workitem_type_title);
			typeTitle.setText(entry.getTypeTitle());

			TextView id = (TextView) getView().findViewById(R.id.workitem_id);
			id.setText(entry.getIdentifier());

			ViewSwitcher summarySwitcher = (ViewSwitcher)getView().findViewById(R.id.workitem_detail_summary_switcher);		    
			((TextView)summarySwitcher.findViewById(R.id.workitem_detail_summary_view)).setText(entry.getTitle());
			EditText summaryEditText = (EditText)summarySwitcher.findViewById(R.id.workitem_detail_summary_edit);
			summaryEditText.setText(entry.getTitle());	
			summaryEditText.setSelection(entry.getTitle().length());

			ImageView editIcon = (ImageView) getView().findViewById(R.id.workitem_detail_edit_summary_icon);
			editIcon.setOnTouchListener(new EditTouchListener(summarySwitcher));

			ImageView stateIconUrl = (ImageView) getView().findViewById(R.id.workitem_state_icon);
			UrlImageViewHelper.setUrlDrawable(stateIconUrl, entry.getStateIconUrl());

			TextView stateTitle = (TextView) getView().findViewById(R.id.workitem_state_title);
			stateTitle.setText(entry.getStateTitle());

			ImageView typeIconUrl2 = (ImageView) getView().findViewById(R.id.workitem_type_icon2);
			UrlImageViewHelper.setUrlDrawable(typeIconUrl2, entry.getTypeIconUrl());

			TextView typeTitle2 = (TextView) getView().findViewById(R.id.workitem_type_title2);
			typeTitle2.setText(entry.getTypeTitle());

			TextView filedAgainst = (TextView) getView().findViewById(R.id.workitem_filed_against);
			filedAgainst.setText(entry.getFiledAgainst());

			ImageView severityIconUrl = (ImageView) getView().findViewById(R.id.workitem_severity_icon);
			UrlImageViewHelper.setUrlDrawable(severityIconUrl, entry.getSeverityIconUrl());

			TextView severityTitle = (TextView) getView().findViewById(R.id.workitem_severity_title);
			severityTitle.setText(entry.getSeverityTitle());

			((TextView) getView().findViewById(R.id.workitem_planned_for)).setText(entry.getPlannedFor());

		}
		//mostramos el resultado
		super.setContentShown(true);
	}

	@Override
	public void onLoaderReset(Loader<RTCData<WorkItemEntry>> arg0) {
	}

	
	public class EditTouchListener implements OnTouchListener {

		private ViewSwitcher switcher;
		
		public EditTouchListener(ViewSwitcher switcher) {
			this.switcher = switcher;
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_UP:
				switcher.showNext();				
				return true;

			case MotionEvent.ACTION_DOWN:
				return true;

			default:
				return false;
			}
		}
	}

	/**
	 * LOADER
	 */
	public static class WorkItemLoader extends RTCLoader<WorkItemEntry> {

		public WorkItemLoader(Context context) {
			super(context);

		}

		/**
		 * This is where the bulk of our work is done. This function is called in a background thread and should generate a new set of data to be published by
		 * the loader.
		 */
		@Override
		public RTCData<WorkItemEntry> loadInBackground() {
			// Retrieve user queries
			RTCClientApplication appState = ((RTCClientApplication) this.getContext().getApplicationContext());

			RTCData<WorkItemEntry> data = OSLCUtils.getWorkItemDetail(appState);
			if (data.getResult() == RTCResult.OK && data.getList().size() != 1) {
				data.setResult(RTCResult.REPOSITORY_ERROR);
				data.setTitle(R.string.new_connection_dialog_message_repository_error);
			}
			return data;
		}
	}
}
