package com.elmolyna.rtcclient.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.elmolyna.rtcclient.R;
import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.ResultQueryActivity;
import com.elmolyna.rtcclient.data.QueryEntry;
import com.elmolyna.rtcclient.generic.RTCListFragment;

public abstract class QueriesFragment extends RTCListFragment<QueryEntry> {

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Give some text to display if there is no data. In a real
		// application this would come from a resource.
		this.setEmptyText(getText(R.string.queries_empty_list));

		// Create an empty adapter we will use to display the loaded data.
		adapter = new QueriesAdapter(getActivity());
		setListAdapter(adapter);

		// Necesario para hacer la lista recargable
		getListView().setOnScrollListener(this);

		// Start out with a progress indicator.
		setListShown(false);

		// Prepare the loader. Either re-connect with an existing one, or start a new one.
		getLoaderManager().restartLoader(0, null, this);
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		QueryEntry item = (QueryEntry) l.getItemAtPosition(position);

		RTCClientApplication appState = (RTCClientApplication) getActivity().getApplicationContext();
		appState.setQueryURL(item.getResult() + "?");

		Intent intent = new Intent(this.getActivity(), ResultQueryActivity.class);
		startActivity(intent);
	}

	public class QueriesFavoriteTouchListener implements OnTouchListener {
		private String result;
		private String title;

		public QueriesFavoriteTouchListener(String title, String result) {
			this.result = result;
			this.title = title;
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_UP:

				RTCClientApplication appState = ((RTCClientApplication) v.getContext().getApplicationContext());
				String base64ProjectAreaId = Base64.encodeToString(appState.getProjectAreaDetail().getBytes(), Base64.DEFAULT);
				SharedPreferences preferencias = v.getContext().getSharedPreferences("favoriteQueries_" + base64ProjectAreaId, Context.MODE_PRIVATE);
				String resultFavorite = preferencias.getString(this.result, null);
				Editor editor = preferencias.edit();
				if (resultFavorite == null) {
					editor.putString(result, title);
					((ImageView) v).setImageResource(R.drawable.btn_rating_star_off_selected);
				} else {
					editor.remove(result);
					((ImageView) v).setImageResource(R.drawable.btn_rating_star_off_normal);
				}
				editor.commit();
				return true;

			case MotionEvent.ACTION_DOWN:
				((ImageView) v).setImageResource(R.drawable.btn_rating_star_off_pressed);
				return true;

			default:
				return false;
			}
		}
	}

	public class QueriesAdapter extends ArrayAdapter<QueryEntry> {
		private final LayoutInflater mInflater;

		public QueriesAdapter(Context context) {
			super(context, android.R.layout.simple_list_item_2);
			mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		/**
		 * Populate new items in the list.
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view;

			if (convertView == null) {
				// Infla el layout correspondiente a un elemento de la lista
				view = mInflater.inflate(R.layout.query_entry, parent, false);
			} else {
				view = convertView;
			}

			QueryEntry item = getItem(position);
			String result = item.getResult();
			String title = item.getTitle();

			ImageView favicon = (ImageView) view.findViewById(R.id.queries_entry_fav_icon);
			favicon.setOnTouchListener(new QueriesFavoriteTouchListener(title, result));

			RTCClientApplication appState = ((RTCClientApplication) getContext().getApplicationContext());
			String base64ProjectAreaId = Base64.encodeToString(appState.getProjectAreaDetail().getBytes(), Base64.DEFAULT);
			SharedPreferences preferencias = getContext().getSharedPreferences("favoriteQueries_" + base64ProjectAreaId, Context.MODE_PRIVATE);
			String resultFavorite = preferencias.getString(result, null);
			if (resultFavorite != null) {
				favicon.setImageResource(R.drawable.btn_rating_star_off_selected);
			} else {
				favicon.setImageResource(R.drawable.btn_rating_star_off_normal);
			}

			((TextView) view.findViewById(R.id.queries_entry_title)).setText(title);
			((TextView) view.findViewById(R.id.queries_entry_result)).setText(result);
			return view;
		}
	}
}
