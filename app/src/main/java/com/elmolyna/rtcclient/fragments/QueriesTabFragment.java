package com.elmolyna.rtcclient.fragments;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.elmolyna.rtcclient.R;
import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.data.ProjectAreaServicesEntry;
import com.elmolyna.rtcclient.data.RTCData;
import com.elmolyna.rtcclient.data.RTCResult;
import com.elmolyna.rtcclient.fragments.QueriesFragment.QueriesAdapter;
import com.elmolyna.rtcclient.generic.RTCFragment;
import com.elmolyna.rtcclient.generic.RTCLoader;
import com.elmolyna.rtcclient.generic.TabListener;
import com.elmolyna.rtcclient.oslc.OSLCUtils;

public class QueriesTabFragment extends RTCFragment implements LoaderManager.LoaderCallbacks<RTCData<ProjectAreaServicesEntry>> {

	// DONE al vover a este fragmento desde otra tarea, no se mantiene la selección de tabulador -> guardar en appstate
	// DONE la búsqueda se realiza con texto en negro
	// DONE Apertura de actividades en modo no splash
	// DONE Apertura de nuevas actividades con mejora de transición
	// DONE Búsquedas accesibles en todas las pantallas con fragmento independiente
	// TODO Creación de nuevo WorkItem accesible en todas las pantallas
	// DONE tama�o de letra en funci�n de "dp"
	// TODO Mejora de la interfaz de nueva conexi�n
	// TODO Conexión por defecto
	// TODO Añadir comentarios
	// TODO Modificar planificación
	// TODO Modificar prioridad

	private ProjectAreaServicesLoader loader;



	// This is the Adapter being used to display the list's data.
	protected QueriesAdapter adapter;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// We have a menu item to show in action bar.
		setHasOptionsMenu(true);

		// Prepare the loader. Either re-connect with an existing one, or start a new one.
		getLoaderManager().initLoader(0, null, this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// setup action bar for tabs
		ActionBar actionBar = getActivity().getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		Tab tabUser = actionBar
				.newTab()
				.setText(R.string.queries_tab_user)
				.setTabListener(
						new TabListener<QueriesUserFragment>(this.getActivity(), R.id.queries_fragment, "queriesUserFragment", QueriesUserFragment.class, null));
		actionBar.addTab(tabUser);

		Tab tabArea = actionBar
				.newTab()
				.setText(R.string.queries_tab_area)
				.setTabListener(
						new TabListener<QueriesAreaFragment>(this.getActivity(), R.id.queries_fragment, "queriesAreaFragment", QueriesAreaFragment.class, null));
		actionBar.addTab(tabArea);

		Tab tabFavorites = actionBar
				.newTab()
				.setText(R.string.queries_tab_favorites)
				.setTabListener(
						new TabListener<QueriesFavoritesFragment>(this.getActivity(), R.id.queries_fragment, "queriesFavoritesFragment",
								QueriesFavoritesFragment.class, null));
		actionBar.addTab(tabFavorites);
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		// Save the index of the currently selected tab

		int index = getActivity().getActionBar().getSelectedTab().getPosition();
		RTCClientApplication appState = ((RTCClientApplication) getActivity().getApplicationContext());
		appState.setQueriesTabFragmentSelectedTabIndex(index);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		RTCClientApplication appState = ((RTCClientApplication) getActivity().getApplicationContext());
		int index = appState.getQueriesTabFragmentSelectedTabIndex();
		getActivity().getActionBar().setSelectedNavigationItem(index);
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	// LoaderManager.LoaderCallbacks
	@Override
	public Loader<RTCData<ProjectAreaServicesEntry>> onCreateLoader(int id, Bundle args) {
		// This is called when a new Loader needs to be created. This
		// sample only has one Loader with no arguments, so it is simple.
		loader = new ProjectAreaServicesLoader(getActivity());
		return loader;
	}

	// LoaderManager.LoaderCallbacks
	@Override
	public void onLoadFinished(Loader<RTCData<ProjectAreaServicesEntry>> loader, RTCData<ProjectAreaServicesEntry> data) {

		if (data.getResult() != RTCResult.OK) {
			super.showErrorDialog(data.getTitle(), data.getMessage());
		} else {
			RTCClientApplication appState = ((RTCClientApplication) getActivity().getApplicationContext());
			appState.setServices(data.getList().get(0));
		}
	}

	// LoaderManager.LoaderCallbacks
	@Override
	public void onLoaderReset(Loader<RTCData<ProjectAreaServicesEntry>> loader) {

	}

	/**
	 * LOADER
	 */
	public static class ProjectAreaServicesLoader extends RTCLoader<ProjectAreaServicesEntry> {

		public ProjectAreaServicesLoader(Context context) {
			super(context);
		}

		/**
		 * This is where the bulk of our work is done. This function is called in a background thread and should generate a new set of data to be published by
		 * the loader.
		 */
		@Override
		public RTCData<ProjectAreaServicesEntry> loadInBackground() {
			// Retrieve user queries
			RTCClientApplication appState = ((RTCClientApplication) this.getContext().getApplicationContext());
			RTCData<ProjectAreaServicesEntry> data = OSLCUtils.getProjectAreaServices(appState);

			if (data.getResult() == RTCResult.OK) {
				if (data.getList().size() != 1) {
					data.setResult(RTCResult.REPOSITORY_ERROR);
					data.setTitle(R.string.new_connection_dialog_message_repository_error);
				}
			}
			return data;
		}
	}
}
