package com.elmolyna.rtcclient.fragments;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.elmolyna.rtcclient.R;
import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.data.RTCData;
import com.elmolyna.rtcclient.data.WorkItemCommentEntry;
import com.elmolyna.rtcclient.generic.RTCListFragment;
import com.elmolyna.rtcclient.generic.RTCLoader;
import com.elmolyna.rtcclient.oslc.OSLCUtils;

public class WorkItemCommentsFragment extends RTCListFragment<WorkItemCommentEntry> {

	private WorkItemCommentsLoader loader;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Give some text to display if there is no data. In a real
		// application this would come from a resource.
		this.setEmptyText(getText(R.string.workitem_comments_empty_list));

		// Create an empty adapter we will use to display the loaded data.
		adapter = new WorkItemCommentsAdapter(getActivity());
		setListAdapter(adapter);

		// Necesario para hacer la lista recargable
		getListView().setOnScrollListener(this);

		// Start out with a progress indicator.
		setListShown(false);

		registerForContextMenu(getListView());

		// Prepare the loader. Either re-connect with an existing one, or start a new one.
		getLoaderManager().initLoader(0, null, this);
	}

	
	// LoaderManager.LoaderCallbacks
	@Override
	public Loader<RTCData<WorkItemCommentEntry>> onCreateLoader(int id, Bundle args) {
		// This is called when a new Loader needs to be created. This
		// sample only has one Loader with no arguments, so it is simple.
		loader = new WorkItemCommentsLoader(getActivity());
		return loader;
	}

	/**
	 * ADAPTER
	 */
	private static SimpleDateFormat dateFormatIn = new  SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	private static SimpleDateFormat dateFormatOut = new  SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	public class WorkItemCommentsAdapter extends ArrayAdapter<WorkItemCommentEntry> {
		private final LayoutInflater mInflater;

		public WorkItemCommentsAdapter(Context context) {
			super(context, android.R.layout.simple_list_item_2);
			mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		/**
		 * Populate new items in the list.
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view;

			if (convertView == null) {
				// Infla el layout correspondiente a un elemento de la lista
				view = mInflater.inflate(R.layout.workitem_comment_entry, parent, false);
			} else {
				view = convertView;
			}

			WorkItemCommentEntry entry = getItem(position);
			
			((TextView) view.findViewById(R.id.workitem_comment_entry_creator)).setText(entry.getCreator());
			
			String dateFormat = "";
			try {
				Date date = dateFormatIn.parse(entry.getCreated());
				dateFormat = dateFormatOut.format(date);
			} catch (ParseException e) {
				dateFormat = "";
			}
			
			((TextView) view.findViewById(R.id.workitem_comment_entry_created)).setText(dateFormat);			
			((TextView) view.findViewById(R.id.workitem_comment_entry_description)).setText(Html.fromHtml(entry.getDescription()));

			return view;
		}
	}

	/**
	 * LOADER
	 */
	public static class WorkItemCommentsLoader extends RTCLoader<WorkItemCommentEntry> {

		public WorkItemCommentsLoader(Context context) {
			super(context);
		}

		/**
		 * This is where the bulk of our work is done. This function is called in a background thread and should generate a new set of data to be published by
		 * the loader.
		 */
		@Override
		public RTCData<WorkItemCommentEntry> loadInBackground() {
			// Retrieve user queries
			RTCClientApplication appState = ((RTCClientApplication) this.getContext().getApplicationContext());
			return OSLCUtils.getWorkItemComments(appState);
		}
	}
}
