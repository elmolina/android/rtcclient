package com.elmolyna.rtcclient.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.Loader;
import android.text.Html;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.elmolyna.rtcclient.R;
import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.WorkItemActivity;
import com.elmolyna.rtcclient.data.RTCData;
import com.elmolyna.rtcclient.data.ResultQueryEntry;
import com.elmolyna.rtcclient.generic.RTCListFragment;
import com.elmolyna.rtcclient.generic.RTCLoader;
import com.elmolyna.rtcclient.oslc.OSLCUtils;
import com.elmolyna.rtcclient.urlimageviewhelper.UrlImageViewHelper;

public class ResultQueryFragment extends RTCListFragment<ResultQueryEntry> {

	// TODO Planned for en lista flotante
	// TODO Ordenación de lista
	// TODO Mostrar padres e hijos

	private ResultQueryLoader loader;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Give some text to display if there is no data. In a real
		// application this would come from a resource.
		this.setEmptyText(getText(R.string.result_query_empty_list));

		// Create an empty adapter we will use to display the loaded data.
		adapter = new ResultQueryAdapter(getActivity());
		setListAdapter(adapter);

		// Necesario para hacer la lista recargable
		getListView().setOnScrollListener(this);

		// Start out with a progress indicator.
		setListShown(false);

		registerForContextMenu(getListView());
		// We have a menu item to show in action bar.
		setHasOptionsMenu(true);

		// Prepare the loader. Either re-connect with an existing one, or start a new one.
		getLoaderManager().initLoader(0, null, this);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		ResultQueryEntry entry = adapter.getItem(info.position);

		FragmentTransaction transaction = getFragmentManager().beginTransaction();

		ResultQueryFragment fragment = new ResultQueryFragment();

		RTCClientApplication appState = ((RTCClientApplication) getActivity().getApplicationContext());

		switch (item.getItemId()) {
		case R.string.result_query_context_menu_parent:
			appState.setQueryURL(entry.getParentUrl() + "?");
			break;
		case R.string.result_query_context_menu_children:
			appState.setQueryURL(entry.getChildrenUrl() + "?");
			break;
		}

		// Replace whatever is in the fragment_container view with this fragment,
		// and add the transaction to the back stack
		transaction.replace(R.id.result_query_fragment, fragment);
		transaction.addToBackStack(null);

		// Commit the transaction
		transaction.commit();

		// resultQueryListener.onRelationItemClick(entry.getParent());
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		ResultQueryEntry entry = adapter.getItem(info.position);
		if (entry.isHasParent()) {
			menu.add(Menu.NONE, R.string.result_query_context_menu_parent, 1, R.string.result_query_context_menu_parent);
			menu.setHeaderTitle(entry.getIdentifier());
		}
		if (entry.isHasChildrem()) {
			menu.add(Menu.NONE, R.string.result_query_context_menu_children, 2, R.string.result_query_context_menu_children);
			menu.setHeaderTitle(entry.getIdentifier());
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		ResultQueryEntry item = (ResultQueryEntry) l.getItemAtPosition(position);
		RTCClientApplication appState = ((RTCClientApplication) this.getActivity().getApplicationContext());
		appState.setWorkitemURL(item.getUrl());
		appState.setWorkitemTabFragmentSelectedTabIndex(0);
		Intent intent = new Intent(this.getActivity(), WorkItemActivity.class);
		startActivity(intent);
	}

	// LoaderManager.LoaderCallbacks
	@Override
	public Loader<RTCData<ResultQueryEntry>> onCreateLoader(int id, Bundle args) {
		// This is called when a new Loader needs to be created. This
		// sample only has one Loader with no arguments, so it is simple.
		String next = null;
		if (args != null)
			next = args.getString("NEXT");
		loader = new ResultQueryLoader(getActivity(), next);
		return loader;
	}

	/**
	 * ADAPTER
	 */

	public class ResultQueryAdapter extends ArrayAdapter<ResultQueryEntry> {
		private final LayoutInflater mInflater;

		public ResultQueryAdapter(Context context) {
			super(context, android.R.layout.simple_list_item_2);
			mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		/**
		 * Populate new items in the list.
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view;

			if (convertView == null) {
				// Infla el layout correspondiente a un elemento de la lista
				view = mInflater.inflate(R.layout.result_query_entry, parent, false);
			} else {
				view = convertView;
			}

			ResultQueryEntry entry = getItem(position);
			ImageView typeIconUrl = (ImageView) view.findViewById(R.id.result_query_entry_type_icon);
			UrlImageViewHelper.setUrlDrawable(typeIconUrl, entry.getTypeIconUrl());

			((TextView) view.findViewById(R.id.result_query_entry_id)).setText(entry.getIdentifier());
			((TextView) view.findViewById(R.id.result_query_entry_title)).setText(Html.fromHtml(entry.getTitle()));

			String planned_for = entry.getPlannedFor();
			if (planned_for != null && planned_for != "") {
				((TextView) view.findViewById(R.id.result_query_entry_planned_for)).setText(entry.getPlannedFor());
			}

			if (entry.isHasParent()) {
				((TextView) view.findViewById(R.id.result_query_entry_parent_count)).setText("(1)");
			}

			if (entry.isHasChildrem()) {
				((TextView) view.findViewById(R.id.result_query_entry_child_count)).setText("(" + entry.getChildrenNumber() + ")");
			}

			((TextView) view.findViewById(R.id.result_query_entry_workitem_url)).setText(entry.getUrl());

			return view;
		}
	}

	/**
	 * LOADER
	 */
	public static class ResultQueryLoader extends RTCLoader<ResultQueryEntry> {

		private String next = null;

		public ResultQueryLoader(Context context, String next) {
			super(context);
			this.next = next;
		}

		/**
		 * This is where the bulk of our work is done. This function is called in a background thread and should generate a new set of data to be published by
		 * the loader.
		 */
		@Override
		public RTCData<ResultQueryEntry> loadInBackground() {
			// Retrieve user queries
			RTCClientApplication appState = ((RTCClientApplication) this.getContext().getApplicationContext());
			if (next == null)
				return OSLCUtils.getQueryResult(appState, appState.getQueryURL());
			return OSLCUtils.getNextQueryResult(appState, next);
		}
	}
}
