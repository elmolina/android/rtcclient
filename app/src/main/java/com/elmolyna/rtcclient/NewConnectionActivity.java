package com.elmolyna.rtcclient;

import java.util.HashSet;
import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.elmolyna.rtcclient.data.ProjectAreaEntry;
import com.elmolyna.rtcclient.data.RTCData;
import com.elmolyna.rtcclient.data.RTCResult;
import com.elmolyna.rtcclient.data.RootServiceEntry;
import com.elmolyna.rtcclient.oslc.OSLCUtils;

public class NewConnectionActivity extends Activity {

	//TODO En caso de que el usuario / password están mal no se muestra el error de autenticacion
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_connection);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_connection_options_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.new_connection_options_menu_connect:
			String new_connection_url = ((EditText) findViewById(R.id.new_connection_url)).getText().toString();

			if (new_connection_url == null || new_connection_url.length() == 0) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(R.string.new_connection_dialog_connection_null_title);
				builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						((EditText) findViewById(R.id.new_connection_url)).requestFocus();
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
				return true;
			}

			String new_connection_id = ((EditText) findViewById(R.id.new_connection_id)).getText().toString();

			if (new_connection_id == null || new_connection_id.length() == 0) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(R.string.new_connection_dialog_id_null_title);
				builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						((EditText) findViewById(R.id.new_connection_id)).requestFocus();
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
				return true;
			}

			String new_connection_username = ((EditText) findViewById(R.id.new_connection_username)).getText().toString();
			String new_connection_password = ((EditText) findViewById(R.id.new_connection_password)).getText().toString();

			// Gets the URL from the UI's text field.
			ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isConnected()) {
				new TestConnection (new_connection_url, new_connection_id, new_connection_username, new_connection_password).execute();
			} else {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(R.string.new_connection_dialog_message_no_network).setTitle(R.string.new_connection_dialog_title_no_network);
				builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	// Uses AsyncTask to create a task away from the main UI thread. This task takes a
	// URL string and uses it to create an HttpUrlConnection. Once the connection
	// has been established, the AsyncTask downloads the contents of the webpage as
	// an InputStream. Finally, the InputStream is converted into a string, which is
	// displayed in the UI by the AsyncTask's onPostExecute method.
	private class TestConnection extends AsyncTask<Void, Void, RTCData<ProjectAreaEntry>> {

		private ProgressDialog progressDialog = null;

		private String new_connection_url = null;
		private String new_connection_id = null;
		private String new_connection_username = null;
		private String new_connection_password = null;

		
		public TestConnection (String new_connection_url, String new_connection_id, String new_connection_username, String new_connection_password){
			this.new_connection_url = new_connection_url;
			this.new_connection_id = new_connection_id;
			this.new_connection_username =new_connection_username;
			this.new_connection_password = new_connection_password;
		}
		
		@Override
		protected void onPreExecute() {

			progressDialog = new ProgressDialog(NewConnectionActivity.this, ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
		};

		@Override
		protected void onPostExecute(RTCData<ProjectAreaEntry> result) {
			super.onPostExecute(result);
			progressDialog.dismiss();

			if (result.getResult() == RTCResult.OK) {
				// Guardamos las preferencias y establecemos esta conexión como la de por defecto
				SharedPreferences preferencias = getSharedPreferences("datos", Context.MODE_PRIVATE);
				Editor editor = preferencias.edit();
				Set<String> ids = preferencias.getStringSet("ids", new HashSet<String>());
				ids.add(new_connection_id);

				editor.putStringSet("ids", ids);
				editor.putString(new_connection_id + "new_connection_url", new_connection_url);
				editor.putString(new_connection_id + "new_connection_username", new_connection_username);
				editor.putString(new_connection_id + "new_connection_password", new_connection_password);
				editor.commit();

				AlertDialog.Builder builder = new AlertDialog.Builder(NewConnectionActivity.this);
				builder.setMessage(R.string.new_connection_dialog_message_test_ok).setTitle(R.string.new_connection_dialog_title_test_ok);
				builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Después de hacer OK, volvemos a la pantala de selección de conexiones
						Intent intent = new Intent(NewConnectionActivity.this, ConnectionActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						NewConnectionActivity.this.finish();
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			} else {
				AlertDialog.Builder builder = new AlertDialog.Builder(NewConnectionActivity.this);
				builder.setMessage(result.getMessage()).setTitle(result.getTitle());
				builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		}

		@Override
		protected RTCData<ProjectAreaEntry> doInBackground(Void... args) {
			RTCClientApplication appState = ((RTCClientApplication) getApplicationContext());
			RTCData<RootServiceEntry> rootServiceData = OSLCUtils.getServiceProvider(appState, new_connection_url, new_connection_username, new_connection_password);
			RTCData<ProjectAreaEntry> projectAreaData = new RTCData<ProjectAreaEntry>();
			if (rootServiceData.getResult() == RTCResult.OK) {
				if (rootServiceData.getList().size() == 1) {
					String serviceProvider = rootServiceData.getList().get(0).getServiceProvider();
					projectAreaData = OSLCUtils.getProjectAreas(appState, serviceProvider, new_connection_url, new_connection_username, new_connection_password);
				} else {
					projectAreaData.setResult(RTCResult.REPOSITORY_ERROR);
					projectAreaData.setTitle(R.string.new_connection_dialog_message_repository_error);
				}
			} else {
				projectAreaData.setResult(rootServiceData.getResult());
				projectAreaData.setTitle(rootServiceData.getTitle());
				projectAreaData.setMessage(rootServiceData.getMessage());
			}
			return projectAreaData;
		}
	}
}
