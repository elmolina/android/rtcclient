package com.elmolyna.rtcclient.oslc;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.elmolyna.rtcclient.data.RootServiceEntry;

public class RootServiceHandler extends RTCHandler<RootServiceEntry>  {

	private RootServiceEntry entry;

	/**
	 * This gets called at the start of an element. Here we're also setting the booleans to true if it's at that specific tag. (so we know where we are)
	 * 
	 * @param namespaceURI
	 * @param localName
	 * @param qName
	 * @param atts
	 * @throws SAXException
	 */
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		if (qName.equals("oslc_cm:cmServiceProviders")) {
			entry = new RootServiceEntry();
			entry.setServiceProvider(atts.getValue("rdf:resource"));
			super.getRTCData().getList().add(entry);
		}
	}
}
