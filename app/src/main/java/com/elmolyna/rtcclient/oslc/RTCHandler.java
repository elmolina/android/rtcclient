package com.elmolyna.rtcclient.oslc;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.elmolyna.rtcclient.data.RTCData;

public class RTCHandler<D> extends DefaultHandler {

	// this holds the data

	private RTCData<D> data = new RTCData<D>();;
	
	/**
	 * Returns the data object
	 * 
	 * @return
	 */
	public RTCData<D> getRTCData() {
		return this.data;
	}
	
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		if (qName.equals("oslc_cm:Collection")) {
			data.setNext(atts.getValue("oslc_cm:next"));
			data.setPrevious(atts.getValue("oslc_cm:previous"));	
		}
	}
	
	/**
	 * This gets called when the xml document is first opened
	 * 
	 * @throws SAXException
	 */
	@Override
	public void startDocument() throws SAXException {
		data.setList(new ArrayList<D>());
	}
}
