package com.elmolyna.rtcclient.oslc;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.elmolyna.rtcclient.data.WorkItemEntry;

public class WorkItemHandler extends RTCHandler<WorkItemEntry> {

	// booleans that check whether it's in a specific tag or not
	private boolean filedAgainst, title, identifier, plannedFor, parent, type, iconUrl, priority, state, severity, description;

	private WorkItemEntry entry;

	/**
	 * Called when it's finished handling the document
	 * 
	 * @throws SAXException
	 */
	@Override
	public void endDocument() throws SAXException {

	}

	/**
	 * This gets called at the start of an element. Here we're also setting the booleans to true if it's at that specific tag. (so we know where we are)
	 * 
	 * @param namespaceURI
	 * @param localName
	 * @param qName
	 * @param atts
	 * @throws SAXException
	 */
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		if (qName.equals("oslc_cm:ChangeRequest")) {
			entry = new WorkItemEntry();
			entry.setWorkitemURL(atts.getValue("rdf:resource"));
		} else if (qName.equals("rtc_cm:filedAgainst"))
			filedAgainst = true;
		else if (qName.equals("dc:title"))
			title = true;
		else if (qName.equals("dc:identifier"))
			identifier = true;
		else if (qName.equals("rtc_cm:plannedFor"))
			plannedFor = true;
		else if (qName.equals("dc:type"))
			type = true;
		else if (qName.equals("rtc_cm:iconUrl"))
			iconUrl = true;
		else if (qName.equals("oslc_cm:priority"))
			priority = true;
		else if (qName.equals("rtc_cm:state"))
			state = true;
		else if (qName.equals("oslc_cm:severity"))
			severity = true;
		else if (qName.equals("dc:description"))
			description = true;
		else if (qName.equals("rtc_cm:com.ibm.team.workitem.linktype.parentworkitem.parent")) {
			if (parent)
				entry.setHasParent(true);
			else
				entry.setParent(atts.getValue("oslc_cm:collref"));
			parent = true;
		}

	}

	/**
	 * Called at the end of the element. Setting the booleans to false, so we know that we've just left that tag.
	 * 
	 * @param namespaceURI
	 * @param localName
	 * @param qName
	 * @throws SAXException
	 */
	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		if (qName.equals("oslc_cm:ChangeRequest"))
			super.getRTCData().getList().add(entry);
		else if (qName.equals("rtc_cm:filedAgainst"))
			filedAgainst = false;
		else if (qName.equals("dc:title"))
			title = false;
		else if (qName.equals("dc:identifier"))
			identifier = false;
		else if (qName.equals("rtc_cm:plannedFor"))
			plannedFor = false;
		else if (qName.equals("dc:type"))
			type = false;
		else if (qName.equals("rtc_cm:iconUrl"))
			iconUrl = false;
		else if (qName.equals("oslc_cm:priority"))
			priority = false;
		else if (qName.equals("rtc_cm:state"))
			state = false;
		else if (qName.equals("oslc_cm:severity"))
			severity = false;
		else if (qName.equals("dc:description"))
			description = false;
		else if (qName.equals("rtc_cm:com.ibm.team.workitem.linktype.parentworkitem.parent"))
			parent = false;
	}

	/**
	 * Calling when we're within an element. Here we're checking to see if there is any content in the tags that we're interested in and populating it in the
	 * Config object.
	 * 
	 * @param ch
	 * @param start
	 * @param length
	 */
	@Override
	public void characters(char ch[], int start, int length) {
		String chars = new String(ch, start, length);
		chars = chars.trim();

		if (filedAgainst && title)
			entry.addFiledAgainst(chars);
		else if (plannedFor && title)
			entry.addPlannedFor(chars);
		else if (plannedFor && title)
			entry.addPlannedFor(chars);
		else if (type && title)
			entry.addTypeTitle(chars);
		else if (type && iconUrl)
			entry.addTypeIconUrl(chars);
		else if (priority && iconUrl)
			entry.addPriorityIconUrl(chars);
		else if (priority && title)
			entry.addPriorityTitle(chars);
		else if (state && iconUrl)
			entry.addStateIconUrl(chars);
		else if (state && title)
			entry.addStateTitle(chars);
		else if (severity && iconUrl)
			entry.addSeverityIconUrl(chars);
		else if (severity && title)
			entry.addSeverityTitle(chars);
		else if (title)
			entry.addTitle(chars);
		else if (identifier)
			entry.addIdentifier(chars);
		else if (description)
			entry.addDescription(chars);

	}
}
