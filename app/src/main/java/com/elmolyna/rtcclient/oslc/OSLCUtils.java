package com.elmolyna.rtcclient.oslc;

import java.io.IOException;
import java.net.ConnectException;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpResponse;
import org.apache.http.auth.InvalidCredentialsException;
import org.apache.http.client.methods.HttpGet;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import com.elmolyna.rtcclient.R;
import com.elmolyna.rtcclient.RTCClientApplication;
import com.elmolyna.rtcclient.data.ProjectAreaDetailEntry;
import com.elmolyna.rtcclient.data.ProjectAreaEntry;
import com.elmolyna.rtcclient.data.ProjectAreaServicesEntry;
import com.elmolyna.rtcclient.data.QueryEntry;
import com.elmolyna.rtcclient.data.RTCData;
import com.elmolyna.rtcclient.data.RTCResult;
import com.elmolyna.rtcclient.data.ResultQueryEntry;
import com.elmolyna.rtcclient.data.RootServiceEntry;
import com.elmolyna.rtcclient.data.WorkItemCommentEntry;
import com.elmolyna.rtcclient.data.WorkItemEntry;

public class OSLCUtils {

	private static String PAGESIZE = "20";
	public static int THRESSHOLD = 15;

	// For Testing connection
	public static RTCData<RootServiceEntry> getServiceProvider(RTCClientApplication appState, String new_connection_url, String new_connection_username,
			String new_connection_password) {
		// Setup the rootServices request
		HttpGet doc = new HttpGet(new_connection_url + "/rootservices");
		doc.addHeader("Accept", "application/rdf+xml");
		doc.addHeader("OSLC-Core-Version", "2.0");

		return getAndSax(appState, new_connection_url, new_connection_username, new_connection_password, doc, new RootServiceHandler());
	}

	public static RTCData<RootServiceEntry> getServiceProvider(RTCClientApplication appState) {
		String urlStr = appState.getConnectionUrl() + "/rootservices";

		// Setup the rootServices request
		HttpGet doc = new HttpGet(urlStr);
		doc.addHeader("Accept", "application/rdf+xml");
		doc.addHeader("OSLC-Core-Version", "2.0");

		return getAndSax(appState, appState.getConnectionUrl(), appState.getUsername(), appState.getPassword(), doc, new RootServiceHandler());
	}

	public static RTCData<ProjectAreaEntry> getProjectAreas(RTCClientApplication appState, String serviceProvider, String new_connection_url,
			String new_connection_username, String new_connection_password) {

		// https://jazz.net/jazz/oslc/workitems/catalog
		HttpGet doc = new HttpGet(serviceProvider);
		doc.addHeader("Accept", "application/xml");
		doc.addHeader("OSLC-Core-Version", "2.0");

		return getAndSax(appState, new_connection_url, new_connection_username, new_connection_password, doc, new ProjectAreasHandler());
	}

	public static RTCData<ProjectAreaEntry> getProjectAreas(RTCClientApplication appState, String serviceProvider) {

		// https://jazz.net/jazz/oslc/workitems/catalog
		HttpGet doc = new HttpGet(serviceProvider);
		doc.addHeader("Accept", "application/xml");
		doc.addHeader("OSLC-Core-Version", "2.0");

		return getAndSax(appState, appState.getConnectionUrl(), appState.getUsername(), appState.getPassword(), doc, new ProjectAreasHandler());
	}

	public static RTCData<ProjectAreaDetailEntry> getProjectAreaDetail(RTCClientApplication appState, String detailUrl) {
		// https://jazz.net/jazz/process/project-areas/_0rSUcMixEd6A25wBGCmItw

		HttpGet doc = new HttpGet(detailUrl);
		// Request the Root Services document
		doc.addHeader("Accept", "application/xml");
		return getAndSax(appState, appState.getConnectionUrl(), appState.getUsername(), appState.getPassword(), doc, new ProjectAreaDetailHandler());
	}

	public static RTCData<ProjectAreaServicesEntry> getProjectAreaServices(RTCClientApplication appState) {
		// https://jazz.net/jazz/oslc/queries.xml?oslc_cm.query=rtc_cm:projectArea="_iMYpMoWtEeGYLb4pp0k1_g" and dc:creator="{currentUser}"
		String urlStr = appState.getProjectAreaServices();

		HttpGet doc = new HttpGet(urlStr);
		// Request the Root Services document
		doc.addHeader("Accept", "application/xml");
		return getAndSax(appState, appState.getConnectionUrl(), appState.getUsername(), appState.getPassword(), doc, new ProjectAreaServicesHandler());
	}

	public static RTCData<QueryEntry> getUserQueries(RTCClientApplication appState) {
		String projectAreaDetail = appState.getProjectAreaDetail();
		String projectAreaID = projectAreaDetail.substring(projectAreaDetail.lastIndexOf('/') + 1);
		// https://jazz.net/jazz/oslc/queries.xml?oslc_cm.query=rtc_cm:projectArea="_iMYpMoWtEeGYLb4pp0k1_g" and dc:creator="{currentUser}"
		String urlStr = appState.getConnectionUrl() + "/oslc/queries.xml?oslc_cm.query=rtc_cm:projectArea=%22" + projectAreaID
				+ "%22%20and%20dc:creator=%22%7BcurrentUser%7D%22" + "&oslc_cm.properties=" + "dc:title," + "rtc_cm:results";

		HttpGet doc = new HttpGet(urlStr);
		doc.addHeader("Accept", "application/xml");
		// Request the Root Services document
		return getAndSax(appState, appState.getConnectionUrl(), appState.getUsername(), appState.getPassword(), doc, new QueriesHandler());
	}

	public static RTCData<QueryEntry> getNextQueries(RTCClientApplication appState, String query) {
		// https://jazz.net/jazz/oslc/queries?oslc_cm.pageSize=4&oslc_cm.properties=dc:title,rtc_cm:results&_resultToken=_ddrnkbQrEeK2AMhrT2UfRQ&_startIndex=4
		HttpGet doc = new HttpGet(query);
		doc.addHeader("Accept", "application/xml");
		// Request the Root Services document
		return getAndSax(appState, appState.getConnectionUrl(), appState.getUsername(), appState.getPassword(), doc, new QueriesHandler());
	}

	public static RTCData<QueryEntry> getAreaQueries(RTCClientApplication appState) {
		String projectAreaDetail = appState.getProjectAreaDetail();
		String projectAreaID = projectAreaDetail.substring(projectAreaDetail.lastIndexOf('/') + 1);

		// https://jazz.net/jazz/oslc/queries.xml?oslc_cm.query=rtc_cm:projectArea="_iMYpMoWtEeGYLb4pp0k1_g"&oslc_cm.properties=dc:title,rtc_cm:results&oslc_cm.pageSize=4
		String urlStr = appState.getConnectionUrl() + "/oslc/queries.xml?oslc_cm.query=rtc_cm:projectArea=%22" + projectAreaID + "%22" + "&oslc_cm.properties="
				+ "dc:title," + "rtc_cm:results" + "&oslc_cm.pageSize=" + PAGESIZE;

		HttpGet doc = new HttpGet(urlStr);
		doc.addHeader("Accept", "application/xml");
		// Request the Root Services document
		return getAndSax(appState, appState.getConnectionUrl(), appState.getUsername(), appState.getPassword(), doc, new QueriesHandler());
	}

	public static RTCData<ResultQueryEntry> getNextQueryResult(RTCClientApplication appState, String next) {
		HttpGet doc = new HttpGet(next);
		doc.addHeader("Accept", "application/xml");
		// Request the Root Services document

		return getAndSax(appState, appState.getConnectionUrl(), appState.getUsername(), appState.getPassword(), doc, new ResultQueryHandler());
	}

	public static RTCData<ResultQueryEntry> getQueryResult(RTCClientApplication appState, String query) {

		// https://jazz.net/jazz/oslc/queries/_PxY5XpGSEeKSpIQ-W8HL4A/rtc_cm:results?oslc_cm.properties=rtc_cm:com.ibm.team.workitem.linktype.parentworkitem.parent{},rtc_cm:com.ibm.team.workitem.linktype.parentworkitem.children{}.dc:title,dc:identifier,rtc_cm:filedAgainst{dc:title},rtc_cm:plannedFor{dc:title}
		// /sort=dc:identifier

		// https://jazz.net/jazz/oslc/queries/_PxY5XpGSEeKSpIQ-W8HL4A/rtc_cm:results
		String urlStr = query + "oslc_cm.properties=" + "rtc_cm:com.ibm.team.workitem.linktype.parentworkitem.parent%7B%7D,"
				+ "rtc_cm:com.ibm.team.workitem.linktype.parentworkitem.children%7B%7D," + "dc:title," + "dc:identifier,"
				+ "rtc_cm:filedAgainst%7Bdc:title%7D," + "dc:type%7Brtc_cm:iconUrl%7D," + "rtc_cm:plannedFor%7Bdc:title%7D" + "&oslc_cm.pageSize=" + PAGESIZE;

		HttpGet doc = new HttpGet(urlStr);
		doc.addHeader("Accept", "application/xml");
		// Request the Root Services document

		return getAndSax(appState, appState.getConnectionUrl(), appState.getUsername(), appState.getPassword(), doc, new ResultQueryHandler());
	}

	public static RTCData<WorkItemEntry> getWorkItemDetail(RTCClientApplication appState) {

		// https://jazz.net/jazz/resource/itemName/com.ibm.team.workitem.WorkItem/206707?oslc_cm.properties=rtc_cm:com.ibm.team.workitem.linktype.parentworkitem.parent%7B%7D,dc:title,dc:identifier,rtc_cm:filedAgainst%7Bdc:title%7D,rtc_cm:plannedFor%7Bdc:title%7D%22,dc:type%7Bdc:title,rtc_cm:iconUrl%7D
		String urlStr = appState.getWorkitemURL() + "?oslc_cm.properties=" + "rtc_cm:com.ibm.team.workitem.linktype.parentworkitem.parent%7B%7D,"
				+ "rtc_cm:com.ibm.team.workitem.linktype.parentworkitem.children%7B%7D," + "dc:title," + "dc:identifier,"
				+ "rtc_cm:filedAgainst%7Bdc:title%7D," + "rtc_cm:plannedFor%7Bdc:title%7D," + "dc:type%7Bdc:title,rtc_cm:iconUrl%7D"
				+ "oslc_cm:priority%7Bdc:title,rtc_cm:iconUrl%7D" + "rtc_cm:state%7Bdc:title,rtc_cm:iconUrl%7D"
				+ "oslc_cm:severity%7Bdc:title,rtc_cm:iconUrl%7D";

		HttpGet doc = new HttpGet(urlStr);
		doc.addHeader("Accept", "application/xml");
		// Request the Root Services document

		return getAndSax(appState, appState.getConnectionUrl(), appState.getUsername(), appState.getPassword(), doc, new WorkItemHandler());
	}

	public static RTCData<WorkItemEntry> getWorkItemDescription(RTCClientApplication appState) {

		// https://jazz.net/jazz/resource/itemName/com.ibm.team.workitem.WorkItem/206707?oslc_cm.properties=rtc_cm:com.ibm.team.workitem.linktype.parentworkitem.parent%7B%7D,dc:title,dc:identifier,rtc_cm:filedAgainst%7Bdc:title%7D,rtc_cm:plannedFor%7Bdc:title%7D%22,dc:type%7Bdc:title,rtc_cm:iconUrl%7D
		String urlStr = appState.getWorkitemURL() + "?oslc_cm.properties=" + "dc:description";

		HttpGet doc = new HttpGet(urlStr);
		doc.addHeader("Accept", "application/xml");
		// Request the Root Services document

		return getAndSax(appState, appState.getConnectionUrl(), appState.getUsername(), appState.getPassword(), doc, new WorkItemHandler());
	}

	public static RTCData<WorkItemCommentEntry> getWorkItemComments(RTCClientApplication appState) {

		// https://jazz.net/jazz/resource/itemName/com.ibm.team.workitem.WorkItem/206707?oslc_cm.properties=rtc_cm:com.ibm.team.workitem.linktype.parentworkitem.parent%7B%7D,dc:title,dc:identifier,rtc_cm:filedAgainst%7Bdc:title%7D,rtc_cm:plannedFor%7Bdc:title%7D%22,dc:type%7Bdc:title,rtc_cm:iconUrl%7D
		String urlStr = appState.getWorkitemURL() + "?oslc_cm.properties=" + "rtc_cm:comments%7Bdc:created,dc:creator%7Bdc:title%7D,dc:description%7D";

		HttpGet doc = new HttpGet(urlStr);
		doc.addHeader("Accept", "application/xml");
		// Request the Root Services document

		return getAndSax(appState, appState.getConnectionUrl(), appState.getUsername(), appState.getPassword(), doc, new WorkItemCommentsHandler());
	}

	private static <D> RTCData<D> getAndSax(RTCClientApplication appState, String server, String username, String password, HttpGet doc, RTCHandler<D> handler) {
		return getAndSax(appState, server, username, password, doc, handler, 0);
	}

	private static <D> RTCData<D> getAndSax(RTCClientApplication appState, String server, String username, String password, HttpGet doc, RTCHandler<D> handler,
			int retry) {

		RTCData<D> data = new RTCData<D>();
		try {
			HttpResponse response = OSLCConnection.sendGetForSecureDocument(server, doc, username, password, appState.getHttpClient());

			if (response.getStatusLine().getStatusCode() == 200) {

				// Define the XPath evaluation environment
				InputSource source = new InputSource(response.getEntity().getContent());

				SAXParserFactory spf = SAXParserFactory.newInstance();
				SAXParser sp = spf.newSAXParser();

				XMLReader xr = sp.getXMLReader();

				xr.setContentHandler(handler);

				xr.parse(source);

				data = handler.getRTCData();
				data.setResult(RTCResult.OK);
			} else {
				data.setResult(RTCResult.IO_ERROR);
				data.setTitle(R.string.new_connection_dialog_message_io_error);
			}
			response.getEntity().consumeContent();

		} catch (SAXException e) {
			e.printStackTrace();
			data.setResult(RTCResult.REPOSITORY_ERROR);
			data.setTitle(R.string.new_connection_dialog_message_repository_error);
			data.setMessage(e.getMessage());
		} catch (InvalidCredentialsException e) {
			e.printStackTrace();
			data.setResult(RTCResult.CREDENTIAL_ERROR);
			data.setTitle(R.string.new_connection_dialog_message_credencial_error);
			data.setMessage(e.getMessage());
		} catch (ConnectException e) {
			if (retry == 0) {
				// En caso de ConnectException, volvemos a intentar la petición reseteando el HttpClient
				appState.restartHttpClient();
				return getAndSax(appState, server, username, password, doc, handler, 1);
			}
			e.printStackTrace();
			data.setResult(RTCResult.IO_ERROR);
			data.setTitle(R.string.new_connection_dialog_message_io_error);
			data.setMessage(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			data.setResult(RTCResult.IO_ERROR);
			data.setTitle(R.string.new_connection_dialog_message_io_error);
			data.setMessage(e.getMessage());
		} catch (Throwable e) {
			e.printStackTrace();
			data.setResult(RTCResult.GENERIC_ERROR);
			data.setTitle(R.string.new_connection_dialog_message_generic_error);
			data.setMessage(e.getMessage());
		}
		return data;
	}
}
