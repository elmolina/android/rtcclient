/*******************************************************************************
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2010. All Rights Reserved. 
 * 
 * Note to U.S. Government Users Restricted Rights:  Use, 
 * duplication or disclosure restricted by GSA ADP Schedule 
 * Contract with IBM Corp.
 *******************************************************************************/
package com.elmolyna.rtcclient.oslc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.InvalidCredentialsException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentProducer;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

/**
 * Factorize some common behaviors shared by the OSLC consumer examples.
 * 
 */
public class OSLCConnection {
	static public boolean DEBUG = true;

	static String AUTHREQUIRED = "X-com-ibm-team-repository-web-auth-msg";

	/**
	 * Print out the HTTPResponse headers
	 */
	public static void printRequestHeaders(HttpGet request) {
		Header[] headers = request.getAllHeaders();
		for (int i = 0; i < headers.length; i++) {
			System.out.println("\t- " + headers[i].getName() + ": " + headers[i].getValue());
		}
	}
	
	/**
	 * Print out the HTTPResponse headers
	 */
	public static void printResponseHeaders(HttpResponse response) {
		Header[] headers = response.getAllHeaders();
		for (int i = 0; i < headers.length; i++) {
			System.out.println("\t- " + headers[i].getName() + ": " + headers[i].getValue());
		}
	}

	/**
	 * Print out the HTTP Response body
	 */
	public static void printResponseBody(HttpResponse response) {
		HttpEntity entity = response.getEntity();
		if (entity == null)
			return;
		BufferedReader reader;
		try {
			reader = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line = reader.readLine();
			while (line != null) {
				System.out.println(line);
				line = reader.readLine();
			}
			reader.close();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Access to a Document protected by a Form based authentication
	 * 
	 * @param serverURI
	 *            - Server URI
	 * @param request
	 *            - HttpGet request
	 * @param login
	 *            - Server login
	 * @param password
	 *            - Server password
	 * @param httpClient
	 *            - HttpClient used for the connection
	 * @param verbose
	 *            - if true, trace all server interactions
	 * @return - HttpResponse
	 * 
	 * @throws IOException
	 * @throws InvalidCredentialsException
	 */
	public static HttpResponse sendGetForSecureDocument(String serverURI, HttpGet request, String login, String password, HttpClient httpClient)
			throws IOException, InvalidCredentialsException {

		// Step (1): Request the protected resource
		if (DEBUG) {			
			System.out.println(">> GET(1) " + request.getURI());
			System.out.println(">> Request Headers:");
			OSLCConnection.printRequestHeaders(request);
		}
			
		HttpResponse documentResponse = httpClient.execute(request);
		if (DEBUG) {
			System.out.println(">> Status Code:" + documentResponse.getStatusLine().getStatusCode());		
			System.out.println(">> Response Headers:");
			OSLCConnection.printResponseHeaders(documentResponse);
		}

		if (documentResponse.getStatusLine().getStatusCode() == 200) {
			Header header = documentResponse.getFirstHeader(AUTHREQUIRED);
			if ((header != null) && ("authrequired".equals(header.getValue()))) {
				documentResponse.getEntity().consumeContent();
				// The server requires an authentication: Create the login form
				HttpPost formPost = new HttpPost(serverURI + "/j_security_check");
				List<NameValuePair> nvps = new ArrayList<NameValuePair>();
				nvps.add(new BasicNameValuePair("j_username", login));
				nvps.add(new BasicNameValuePair("j_password", password));
				formPost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));

				// Step (2): The client submits the login form
				if (DEBUG)
					System.out.println(">> POST " + formPost.getURI());
				HttpResponse formResponse = httpClient.execute(formPost);
				if (DEBUG) {
					System.out.println(">> Status Code:" + documentResponse.getStatusLine().getStatusCode());		
					System.out.println(">> Response Headers:");
					OSLCConnection.printResponseHeaders(formResponse);
				}
				header = formResponse.getFirstHeader(AUTHREQUIRED);
				if ((header != null) && ("authfailed".equals(header.getValue()))) {
					// The login failed
					throw new InvalidCredentialsException("Authentication failed");
				}
				formResponse.getEntity().consumeContent();
				// The login succeed
				// Step (3): Request again the protected resource
				if (DEBUG)
					System.out.println(">> GET(2) " + request.getURI());
				HttpGet documentGet2;
				try {
					documentGet2 = (HttpGet) (request.clone());
					return httpClient.execute(documentGet2);
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}

			}
		}
		return documentResponse;
	}

	/**
	 * Update a Document protected by a Form based authentication
	 * 
	 * @param serverURI
	 *            - Server URI
	 * @param protectedResource
	 *            - Absolute path to the protected document
	 * @param login
	 *            - Server login
	 * @param password
	 *            - Server password
	 * @param httpClient
	 *            - HttpClient used for the connection
	 * @param verbose
	 *            - if true, trace all server interactions
	 * @return - HttpResponse
	 * 
	 * @throws IOException
	 * @throws InvalidCredentialsException
	 */
	public static HttpResponse sendPutForSecureDocument(String serverURI, ContentProducer cp, HttpPut put, String login, String password, HttpClient httpClient)
			throws IOException, InvalidCredentialsException {

		// Step (1): Request the protected resource
		if (DEBUG)
			System.out.println(">> PUT(1) " + put.getURI());
		HttpResponse documentResponse = httpClient.execute(put);
		if (DEBUG) {
			System.out.println(">> Response Headers:");
			OSLCConnection.printResponseHeaders(documentResponse);
		}

		if (documentResponse.getStatusLine().getStatusCode() == 200) {
			Header header = documentResponse.getFirstHeader(AUTHREQUIRED);
			if ((header != null) && ("authrequired".equals(header.getValue()))) {
				documentResponse.getEntity().consumeContent();
				// The server requires an authentication: Create the login form
				HttpPost formPost = new HttpPost(serverURI + "/j_security_check");
				List<NameValuePair> nvps = new ArrayList<NameValuePair>();
				nvps.add(new BasicNameValuePair("j_username", login));
				nvps.add(new BasicNameValuePair("j_password", password));
				formPost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));

				// Step (2): The client submits the login form
				if (DEBUG)
					System.out.println(">> POST " + formPost.getURI());
				HttpResponse formResponse = httpClient.execute(formPost);
				if (DEBUG)
					OSLCConnection.printResponseHeaders(formResponse);

				header = formResponse.getFirstHeader(AUTHREQUIRED);
				if ((header != null) && ("authfailed".equals(header.getValue()))) {
					// The login failed
					throw new InvalidCredentialsException("Authentication failed");
				}
				formResponse.getEntity().consumeContent();
				// The login succeed
				// Step (3): Request again the protected resource
				if (DEBUG)
					System.out.println(">> PUT(2) " + put.getURI());
				// entity = new EntityTemplate(cp);
				// put = new HttpPut(put.getURI());
				// put.addHeader("content-type", contentType);
				// put.setEntity(entity);
				try {
					HttpPut put2 = (HttpPut) put.clone();
					return httpClient.execute(put2);
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
			}
		}
		return documentResponse;
	}

	public static void printRequest(HttpGet request) {
		System.out.println("\t- Method: " + request.getMethod());
		System.out.println("\t- URL: " + request.getURI());
		System.out.println("\t- Headers: ");
		Header[] headers = request.getAllHeaders();
		for (int i = 0; i < headers.length; i++) {
			System.out.println("\t\t- " + headers[i].getName() + ": " + headers[i].getValue());
		}
	}

	public static DocumentBuilder getDocumentParser() {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			dbf.setValidating(false);
			return dbf.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new Error(e);
		}
	}
}
