package com.elmolyna.rtcclient.oslc;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.elmolyna.rtcclient.data.WorkItemCommentEntry;

public class WorkItemCommentsHandler extends RTCHandler<WorkItemCommentEntry> {

	// booleans that check whether it's in a specific tag or not
	private boolean comment, commentlvl2, created, creator, description, title;

	private WorkItemCommentEntry entry;

	/**
	 * Called when it's finished handling the document
	 * 
	 * @throws SAXException
	 */
	@Override
	public void endDocument() throws SAXException {

	}

	/**
	 * This gets called at the start of an element. Here we're also setting the booleans to true if it's at that specific tag. (so we know where we are)
	 * 
	 * @param namespaceURI
	 * @param localName
	 * @param qName
	 * @param atts
	 * @throws SAXException
	 */
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
		if (qName.equals("rtc_cm:comments")) {
			if (comment) {
				entry = new WorkItemCommentEntry();
				commentlvl2 = true;
			} else {
				comment = true;
			}
		} else if (qName.equals("dc:created"))
			created = true;
		else if (qName.equals("dc:title"))
			title = true;
		else if (qName.equals("dc:creator"))
			creator = true;
		else if (qName.equals("dc:description"))
			description = true;
	}

	/**
	 * Called at the end of the element. Setting the booleans to false, so we know that we've just left that tag.
	 * 
	 * @param namespaceURI
	 * @param localName
	 * @param qName
	 * @throws SAXException
	 */
	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
		if (qName.equals("rtc_cm:comments")) {
			if (comment) {
				if (commentlvl2) {
					super.getRTCData().getList().add(entry);
					commentlvl2 = false;
				}
				else {
					comment = false;
				}
			} else {
				comment = false;
			}
		} else if (qName.equals("dc:created"))
			created = false;
		else if (qName.equals("dc:title"))
			title = false;
		else if (qName.equals("dc:creator"))
			creator = false;
		else if (qName.equals("dc:description"))
			description = false;
	}

	/**
	 * Calling when we're within an element. Here we're checking to see if there is any content in the tags that we're interested in and populating it in the
	 * Config object.
	 * 
	 * @param ch
	 * @param start
	 * @param length
	 */
	@Override
	public void characters(char ch[], int start, int length) {
		String chars = new String(ch, start, length);
		chars = chars.trim();

		if (created)
			entry.addCreated(chars);
		else if (creator && title)
			entry.addCreator(chars);
		else if (description)
			entry.addDescription(chars);
	}
}
