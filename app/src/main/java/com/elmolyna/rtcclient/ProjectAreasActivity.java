package com.elmolyna.rtcclient;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.elmolyna.rtcclient.fragments.ProjectAreasFragment;
import com.elmolyna.rtcclient.generic.RTCAnimationActivity;

public class ProjectAreasActivity extends RTCAnimationActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.project_areas_fragment);

		if (savedInstanceState == null) {
			FragmentManager fm = getSupportFragmentManager();
			ProjectAreasFragment projectAreasFragment = new ProjectAreasFragment();
			FragmentTransaction transaction = fm.beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			transaction.add(R.id.project_areas_fragment, projectAreasFragment, "queriesTabFragment");
			transaction.commit();
		}
	}
}
