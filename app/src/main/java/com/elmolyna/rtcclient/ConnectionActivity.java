package com.elmolyna.rtcclient;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.elmolyna.rtcclient.fragments.ConnectionFragment;
import com.elmolyna.rtcclient.generic.RTCActivity;

public class ConnectionActivity extends RTCActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.connection_fragment);
		// Show the Up button in the action bar.
		
		if (savedInstanceState == null) {
			FragmentManager fm = getFragmentManager();
			ConnectionFragment fragment = new ConnectionFragment();
			FragmentTransaction transaction = fm.beginTransaction();
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN) ;
			transaction.add(R.id.connection_fragment, fragment, "connectionFragment");
			transaction.commit();
		}
	}
}